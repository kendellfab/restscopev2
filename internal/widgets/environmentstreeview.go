package widgets

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type EnvironmentsTreeView struct {
	*gtk.TreeView
	treeStore  *EnvironmentsTreeStore
	baseColumn *gtk.TreeViewColumn
}

func NewEnvironmentsTreeView(treeStore *EnvironmentsTreeStore) *EnvironmentsTreeView {
	treeView, err := gtk.TreeViewNew()
	if err != nil {
		panic(err)
	}
	etv := &EnvironmentsTreeView{TreeView: treeView, treeStore: treeStore}
	etv.SetModel(treeStore)
	etv.SetVExpand(true)
	etv.SetActivateOnSingleClick(true)
	etv.SetShowExpanders(true)

	baseRender, _ := gtk.CellRendererTextNew()
	etv.baseColumn, _ = gtk.TreeViewColumnNewWithAttribute("Base", baseRender, "text", 0)
	etv.baseColumn.SetResizable(true)
	etv.AppendColumn(etv.baseColumn)

	childRender, _ := gtk.CellRendererTextNew()
	childColumn, _ := gtk.TreeViewColumnNewWithAttribute("Child", childRender, "text", 1)
	childColumn.SetResizable(true)
	etv.AppendColumn(childColumn)

	return etv
}

func (etv *EnvironmentsTreeView) ActivateFirst() {
	tp, _ := gtk.TreePathNewFromString("0")
	etv.RowActivated(tp, etv.baseColumn)
}

type EnvironmentsTreeStore struct {
	*gtk.TreeStore
}

func newEnvironmentsTreeStore() *EnvironmentsTreeStore {
	treeStore, err := gtk.TreeStoreNew(glib.TYPE_STRING, glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}
	ets := &EnvironmentsTreeStore{TreeStore: treeStore}

	return ets
}

func (ets *EnvironmentsTreeStore) AddBase(env models.Environment) {
	parentIter := ets.Append(nil)
	ets.SetValue(parentIter, 0, env.Name)
}

func (ets *EnvironmentsTreeStore) AddEnvironments(envs []models.Environment) {
	for _, parent := range envs {
		parentIter := ets.Append(nil)
		ets.SetValue(parentIter, 0, parent.Name)
		for _, child := range parent.Children {
			childIter := ets.Append(parentIter)
			ets.SetValue(childIter, 1, child.Name)
		}
	}
}
