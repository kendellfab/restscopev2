package widgets

import (
	"log"

	"github.com/gotk3/gotk3/gtk"
	"github.com/hfmrow/gotk3_gtksource/source"
	"gitlab.com/kendellfab/restscope/internal/callbacks"
	"gitlab.com/kendellfab/restscope/internal/data"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type GetBodyFilePathCallback func(key string) string

type bodyDisplayType int

const (
	bodyNone bodyDisplayType = iota
	keyValuePair
	rawContent
	file
)

type BodyBox struct {
	*gtk.Box
	guiRequestUpdateCallback callbacks.GuiRequestUpdateCallback
	bodyFilePathCallback     GetBodyFilePathCallback
	bodyTypeComboBox         *BodyTypeComboBox
	keyValueList             *KeyValueList
	fileInputBox             *gtk.Box
	fileKey                  *gtk.Entry
	fileEntry                *gtk.Entry
	fileButton               *gtk.Button
	rawBody                  *source.SourceView
	scheme                   *source.SourceStyleScheme
}

func NewBodyBox(guiRequestUpdateCallback callbacks.GuiRequestUpdateCallback, bodyFilePathCallback GetBodyFilePathCallback) *BodyBox {
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	if err != nil {
		panic(err)
	}

	bb := &BodyBox{Box: box, guiRequestUpdateCallback: guiRequestUpdateCallback, bodyFilePathCallback: bodyFilePathCallback}
	bb.bodyTypeComboBox = newBodyTypeComboBox()
	bb.bodyTypeComboBox.SetMarginTop(2)
	bb.bodyTypeComboBox.SetMarginStart(2)
	bb.bodyTypeComboBox.SetMarginEnd(2)
	bb.bodyTypeComboBox.SetMarginBottom(5)
	bb.bodyTypeComboBox.Connect("changed", bb.bodyTypeChanged)
	bb.Add(bb.bodyTypeComboBox)

	return bb
}

func (bb *BodyBox) initKeyValueList() {
	bb.keyValueList = NewKeyValueList(bb.guiRequestUpdateCallback, nil, nil)
	bb.Add(bb.keyValueList)
	bb.keyValueList.ShowAll()
}

func (bb *BodyBox) initRawBody() {
	var err error
	bb.rawBody, err = source.SourceViewNew()
	if err != nil {
		panic(err)
	}
	bb.rawBody.SetShowLineNumbers(true)
	bb.rawBody.SetHExpand(true)
	bb.rawBody.SetVExpand(true)
	bb.rawBody.SetHighlightCurrentLine(true)

	bb.rawBody.Connect("focus-out-event", func() {
		if bb.guiRequestUpdateCallback != nil {
			bb.guiRequestUpdateCallback()
		}
	})

	if bb.scheme != nil {
		bb.updateScheme()
	}

	bb.Add(bb.rawBody)
}

func (bb *BodyBox) initFileBox() {
	var err error
	bb.fileInputBox, err = gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		panic(err)
	}
	bb.fileInputBox.SetMarginStart(5)
	bb.fileInputBox.SetMarginTop(5)
	bb.fileInputBox.SetMarginEnd(5)
	bb.fileInputBox.SetHExpand(true)

	bb.fileKey, err = gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	bb.fileKey.SetHExpand(true)
	bb.fileKey.SetPlaceholderText("Form key for file. Default: file")
	bb.fileInputBox.Add(bb.fileKey)

	bb.fileEntry, err = gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	bb.fileEntry.SetHExpand(true)

	bb.fileButton, err = gtk.ButtonNewWithLabel("Select File")
	if err != nil {
		panic(err)
	}
	bb.fileButton.Connect("clicked", bb.fileButtonClicked)

	bb.fileInputBox.Add(bb.fileEntry)
	bb.fileInputBox.Add(bb.fileButton)
	bb.Add(bb.fileInputBox)
	bb.fileInputBox.ShowAll()
}

func (bb *BodyBox) bodyTypeChanged() {

	if bb.keyValueList == nil {
		bb.initKeyValueList()
	}

	if bb.fileInputBox == nil {
		bb.initFileBox()
	}

	if bb.rawBody == nil {
		bb.initRawBody()
	}

	display := bb.getBodyDisplayType()
	switch display {
	case bodyNone:
		bb.keyValueList.SetVisible(false)
		bb.fileInputBox.SetVisible(false)
		bb.rawBody.SetVisible(false)
	case keyValuePair:
		bb.keyValueList.SetVisible(true)
		bb.fileInputBox.SetVisible(false)
		bb.rawBody.SetVisible(false)
	case file:
		bb.keyValueList.SetVisible(false)
		bb.fileInputBox.SetVisible(true)
		bb.rawBody.SetVisible(false)
	default:
		bb.keyValueList.SetVisible(false)
		bb.fileInputBox.SetVisible(false)
		bb.rawBody.SetVisible(true)
	}
}

func (bb *BodyBox) getBodyDisplayType() bodyDisplayType {
	active := bb.bodyTypeComboBox.GetActive()
	if active == 0 {
		return bodyNone
	} else if active == 1 || active == 2 {
		return keyValuePair
	} else if active == 7 {
		return file
	} else {
		return rawContent
	}
}

func (bb *BodyBox) GetBodyType() data.BodyType {
	active := bb.bodyTypeComboBox.GetActive()
	return data.BodyType(active)
}

func (bb *BodyBox) GetKVPairs() []models.KVPair {
	if bb.keyValueList == nil {
		return nil
	}
	return bb.keyValueList.GetPairs()
}

func (bb *BodyBox) GetRawBody() (string, error) {
	if bb.rawBody == nil {
		return "", nil
	}
	buff, _ := bb.rawBody.GetBuffer()
	start := buff.GetStartIter()
	end := buff.GetEndIter()
	return buff.GetText(start, end, false)
}

func (bb *BodyBox) GetFilePath() (string, error) {
	if bb.fileEntry == nil {
		return "", nil
	}
	return bb.fileEntry.GetText()
}

func (bb *BodyBox) GetFileKey() (string, error) {
	if bb.fileKey == nil {
		return "", nil
	}
	return bb.fileKey.GetText()
}

func (bb *BodyBox) Reset() {
	if bb.keyValueList != nil {
		bb.keyValueList.Reset()
	}
	if bb.rawBody != nil {
		buff, _ := bb.rawBody.GetBuffer()
		buff.SetText("")
	}
	bb.bodyTypeComboBox.SetActive(0)
	if bb.fileEntry != nil {
		bb.fileEntry.SetText("")
	}
	if bb.fileKey != nil {
		bb.fileKey.SetText("")
	}
}

func (bb *BodyBox) updateScheme() {
	if bb.rawBody == nil {
		return
	}
	buff, err := bb.rawBody.GetBuffer()
	if err != nil {
		log.Println("error getting body box source buffer", err)
	} else {
		buff.SetStyleSheme(bb.scheme)
	}
}

func (bb *BodyBox) fileButtonClicked() {
	file := bb.bodyFilePathCallback("file")
	if file != "" {
		bb.fileEntry.SetText(file)

		if bb.guiRequestUpdateCallback != nil {
			bb.guiRequestUpdateCallback()
		}
	}
}

func (bb *BodyBox) SetScheme(scheme *source.SourceStyleScheme) {
	bb.scheme = scheme
	bb.updateScheme()
}

func (bb *BodyBox) UpdateBody(bt data.BodyType, bodyPairs []models.KVPair, bodyRaw string, bodyFilePath string, bodyFileKey string) {
	switch bt {
	case data.BodyNone:
		bb.bodyTypeComboBox.SetActive(0)
	case data.Multipart:
		bb.bodyTypeComboBox.SetActive(1)
		bb.keyValueList.UpdatePairs(bodyPairs)
	case data.Form:
		bb.bodyTypeComboBox.SetActive(2)
		bb.keyValueList.UpdatePairs(bodyPairs)
	case data.JSON:
		bb.bodyTypeComboBox.SetActive(3)
		buff, _ := bb.rawBody.GetBuffer()
		buff.SetText(bodyRaw)
	case data.XML:
		bb.bodyTypeComboBox.SetActive(4)
		buff, _ := bb.rawBody.GetBuffer()
		buff.SetText(bodyRaw)
	case data.YAML:
		bb.bodyTypeComboBox.SetActive(5)
		buff, _ := bb.rawBody.GetBuffer()
		buff.SetText(bodyRaw)
	case data.OTHER:
		bb.bodyTypeComboBox.SetActive(6)
		buff, _ := bb.rawBody.GetBuffer()
		buff.SetText(bodyRaw)
	case data.FILE:
		bb.bodyTypeComboBox.SetActive(7)
		bb.fileEntry.SetText(bodyFilePath)
		bb.fileKey.SetText(bodyFileKey)
	}
}
