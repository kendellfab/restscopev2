package widgets

import (
	"fmt"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type EnvironmentsComboBox struct {
	*gtk.ComboBox
	listStore             *gtk.ListStore
	selectedEnvironmentID int64
	envs                  []models.Environment
}

func NewEnvironmentsCombobox() *EnvironmentsComboBox {
	listStore, err := gtk.ListStoreNew(glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}

	cell, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}

	base, err := gtk.ComboBoxNew()
	if err != nil {
		panic(err)
	}

	envComboBox := &EnvironmentsComboBox{listStore: listStore, ComboBox: base}

	envComboBox.PackStart(cell, true)
	envComboBox.AddAttribute(cell, "text", 0)
	envComboBox.SetModel(envComboBox.listStore)
	envComboBox.SetActive(0)

	return envComboBox
}

func (ecb *EnvironmentsComboBox) AddEnvironments(envs []models.Environment) {
	if len(envs) > 0 {
		selEnv := ecb.GetSelectedEnv()
		if selEnv != nil {
			ecb.selectedEnvironmentID = selEnv.ID
		}
	}
	ecb.listStore.Clear()
	ecb.envs = []models.Environment{}
	for _, env := range envs {
		ecb.envs = append(ecb.envs, env)
		iter := ecb.listStore.Append()
		ecb.listStore.SetValue(iter, 0, env.Name)
		for _, child := range env.Children {
			childIter := ecb.listStore.Append()
			ecb.listStore.SetValue(childIter, 0, fmt.Sprintf("\t%s", child.Name))
		}
	}
	ecb.SetActive(0)
	for i, env := range ecb.envs {
		if env.ID == ecb.selectedEnvironmentID {
			ecb.SetActive(i)
		}
	}
}

func (ecb *EnvironmentsComboBox) GetSelectedEnv() *models.Environment {
	active := ecb.GetActive()
	if active < 0 {
		return nil
	}
	return &ecb.envs[active]
}

func (ecb *EnvironmentsComboBox) Reset() {
	ecb.listStore.Clear()
}
