package widgets

import (
	"log"

	"gitlab.com/kendellfab/restscope/internal/models"

	"github.com/gotk3/gotk3/pango"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/callbacks"
)

type itemUpdatedCallback func()
type doAddPair func(key, value string)

type KeyValueList struct {
	*gtk.Box
	guiRequestUpdateCallback callbacks.GuiRequestUpdateCallback
	pairAddedCallback        callbacks.PairAddedCallback
	treeRowUpdated           callbacks.TreeRowUpdated
	listStore                *KeyValueListStore
	treeView                 *KeyValueTreeView
	addKVPairPopover         *gtk.Popover
}

func NewKeyValueList(callback callbacks.GuiRequestUpdateCallback, pairAddedCallback callbacks.PairAddedCallback, treeRowUpdated callbacks.TreeRowUpdated) *KeyValueList {
	base, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	if err != nil {
		panic(err)
	}

	kvl := &KeyValueList{Box: base, guiRequestUpdateCallback: callback, pairAddedCallback: pairAddedCallback, treeRowUpdated: treeRowUpdated}

	kvl.listStore = newKeyValueListStore()

	buttonBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		panic(err)
	}
	buttonBox.SetMarginTop(5)
	buttonBox.SetMarginBottom(5)
	buttonBox.SetMarginStart(5)

	addButton, err := gtk.ButtonNewFromIconName("list-add", gtk.ICON_SIZE_BUTTON)
	if err != nil {
		panic(err)
	}
	addButton.Connect("clicked", func() {
		log.Println("click add button")
		kvl.addKVPairPopover.ShowAll()
	})
	buttonBox.PackStart(addButton, false, false, 0)

	kvl.addKVPairPopover, err = gtk.PopoverNew(addButton)
	inputBox := newKeyValuePairInputBox(func(key, value string) {
		kvl.listStore.addPair(key, value, true)
		kvl.addKVPairPopover.Hide()
		if kvl.guiRequestUpdateCallback != nil {
			kvl.guiRequestUpdateCallback()
		}
		if kvl.pairAddedCallback != nil {
			kvl.pairAddedCallback()
		}
	})
	kvl.addKVPairPopover.Add(inputBox)

	removeButton, err := gtk.ButtonNewFromIconName("list-remove", gtk.ICON_SIZE_BUTTON)
	if err != nil {
		panic(err)
	}
	removeButton.Connect("clicked", func() {
		sel, _ := kvl.treeView.GetSelection()
		_, iter, ok := sel.GetSelected()
		if ok {
			kvl.listStore.Remove(iter)
		}
	})
	buttonBox.PackStart(removeButton, false, false, 0)

	kvl.Add(buttonBox)

	kvl.treeView = newKeyValueTreeView(kvl.listStore, func() {
		if kvl.treeRowUpdated != nil {
			kvl.treeRowUpdated()
		}
		if kvl.guiRequestUpdateCallback != nil {
			kvl.guiRequestUpdateCallback()
		}
	})
	kvl.treeView.SetVExpand(true)
	kvl.Add(kvl.treeView)

	return kvl
}

func (kvl *KeyValueList) GetPairs() []models.KVPair {
	var res []models.KVPair

	iter, _ := kvl.listStore.GetIterFirst()
	for iter != nil {
		rawKey, _ := kvl.listStore.GetValue(iter, 0)
		rawValue, _ := kvl.listStore.GetValue(iter, 1)
		rawActive, _ := kvl.listStore.GetValue(iter, 2)

		key, _ := rawKey.GoValue()
		value, _ := rawValue.GoValue()
		active, _ := rawActive.GoValue()

		res = append(res, models.KVPair{Key: key.(string), Value: value.(string), Active: active.(int) == 1})

		if done := kvl.listStore.IterNext(iter); !done {
			break
		}
	}

	return res
}

func (kvl *KeyValueList) Reset() {
	kvl.listStore.Clear()
}

func (kvl *KeyValueList) UpdatePairs(pairs []models.KVPair) {
	for _, pair := range pairs {
		kvl.listStore.addPair(pair.Key, pair.Value, pair.Active)
	}
}

type KeyValueListStore struct {
	*gtk.ListStore
}

func newKeyValueListStore() *KeyValueListStore {
	listStore, err := gtk.ListStoreNew(glib.TYPE_STRING, glib.TYPE_STRING, glib.TYPE_INT)
	if err != nil {
		panic(err)
	}

	return &KeyValueListStore{ListStore: listStore}
}

func (ls *KeyValueListStore) addPair(key, value string, active bool) {
	iter := ls.Append()
	ls.SetValue(iter, 0, key)
	ls.SetValue(iter, 1, value)
	isActive := 0
	if active {
		isActive = 1
	}
	ls.SetValue(iter, 2, isActive)
}

type KeyValueTreeView struct {
	*gtk.TreeView
	callback itemUpdatedCallback
}

func newKeyValueTreeView(keyValueListStore *KeyValueListStore, callback itemUpdatedCallback) *KeyValueTreeView {
	treeView, err := gtk.TreeViewNew()
	if err != nil {
		panic(err)
	}

	keyRender, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	keyRender.SetProperty("editable", 1)
	keyRender.ConnectAfter("edited", func(renderer *gtk.CellRendererText, path, data string) {
		tPath, _ := gtk.TreePathNewFromString(path)
		iter, _ := keyValueListStore.GetIter(tPath)
		keyValueListStore.SetValue(iter, 0, data)
		if callback != nil {
			callback()
		}
	})
	keyColumn, err := gtk.TreeViewColumnNewWithAttribute("Name", keyRender, "text", 0)
	if err != nil {
		panic(err)
	}
	treeView.AppendColumn(keyColumn)

	valueRender, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	valueRender.SetProperty("editable", 1)
	valueRender.SetProperty("ellipsize", pango.ELLIPSIZE_END)
	valueRender.ConnectAfter("edited", func(renderer *gtk.CellRendererText, path, data string) {
		tPath, _ := gtk.TreePathNewFromString(path)
		iter, _ := keyValueListStore.GetIter(tPath)
		keyValueListStore.SetValue(iter, 1, data)
		if callback != nil {
			callback()
		}
	})
	valueColumn, err := gtk.TreeViewColumnNewWithAttribute("Value", valueRender, "text", 1)
	if err != nil {
		panic(err)
	}
	valueColumn.SetExpand(true)
	valueColumn.SetResizable(true)
	treeView.AppendColumn(valueColumn)

	activeRender, err := gtk.CellRendererToggleNew()
	if err != nil {
		panic(err)
	}
	activeRender.ConnectAfter("toggled", func(renderer *gtk.CellRendererToggle, path string) {
		tPath, _ := gtk.TreePathNewFromString(path)
		iter, _ := keyValueListStore.GetIter(tPath)
		rawVal, _ := keyValueListStore.GetValue(iter, 2)
		interfaceValue, _ := rawVal.GoValue()
		val := interfaceValue.(int)

		set := 1
		if val == 1 {
			set = 0
		}

		keyValueListStore.SetValue(iter, 2, set)
		if callback != nil {
			callback()
		}
	})
	activeColumn, err := gtk.TreeViewColumnNew()
	if err != nil {
		panic(err)
	}
	activeColumn.SetTitle("Active")
	activeColumn.PackStart(activeRender, false)
	activeColumn.AddAttribute(activeRender, "active", 2)
	activeColumn.SetResizable(true)
	treeView.AppendColumn(activeColumn)

	treeView.SetShowExpanders(true)
	treeView.SetModel(keyValueListStore)

	return &KeyValueTreeView{TreeView: treeView, callback: callback}
}

type KeyValuePairInputBox struct {
	*gtk.Grid
	callback doAddPair
	key      *gtk.Entry
	value    *gtk.Entry
}

func newKeyValuePairInputBox(callback doAddPair) *KeyValuePairInputBox {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}

	grid.SetMarginTop(10)
	grid.SetMarginEnd(10)
	grid.SetMarginBottom(10)
	grid.SetMarginStart(10)

	grid.SetRowSpacing(10)
	grid.SetColumnSpacing(5)

	keyLabel, err := gtk.LabelNew("Key")
	if err != nil {
		panic(err)
	}
	keyEntry, err := gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	keyEntry.SetHExpand(true)
	grid.Attach(keyLabel, 0, 0, 1, 1)
	grid.Attach(keyEntry, 1, 0, 1, 1)

	valueLabel, err := gtk.LabelNew("Value")
	if err != nil {
		panic(err)
	}
	valueEntry, err := gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	valueEntry.SetHExpand(true)
	grid.Attach(valueLabel, 0, 1, 1, 1)
	grid.Attach(valueEntry, 1, 1, 1, 1)

	doAddBtn, err := gtk.ButtonNewWithLabel("Add")
	if err != nil {
		panic(err)
	}
	grid.Attach(doAddBtn, 0, 2, 2, 1)

	doAddBtn.Connect("clicked", func() {
		key, _ := keyEntry.GetText()
		value, _ := valueEntry.GetText()
		callback(key, value)
		keyEntry.SetText("")
		valueEntry.SetText("")
	})

	return &KeyValuePairInputBox{Grid: grid, callback: callback, key: keyEntry, value: valueEntry}
}
