package widgets

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

type ResponseHeadersTreeView struct {
	*gtk.TreeView
	rhls *responseHeadersListStore
}

func newResponseHeadersTreeView() *ResponseHeadersTreeView {
	treeView, err := gtk.TreeViewNew()
	if err != nil {
		panic(err)
	}
	rhtv := &ResponseHeadersTreeView{TreeView: treeView}
	rhtv.rhls = newResponseHeadersListStore()
	rhtv.SetModel(rhtv.rhls)

	keyRender, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	keyColumn, err := gtk.TreeViewColumnNewWithAttribute("Name", keyRender, "text", 0)
	if err != nil {
		panic(err)
	}
	keyColumn.SetResizable(true)
	rhtv.AppendColumn(keyColumn)

	valueRender, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	valueColumn, err := gtk.TreeViewColumnNewWithAttribute("Value", valueRender, "text", 1)
	if err != nil {
		panic(err)
	}
	valueColumn.SetResizable(true)
	rhtv.AppendColumn(valueColumn)

	return rhtv
}

func (rhtv *ResponseHeadersTreeView) reset() {
	rhtv.rhls.Clear()
}

func (rhtv *ResponseHeadersTreeView) addHeaders(headers map[string]string) {
	rhtv.reset()
	for k, v := range headers {
		rhtv.rhls.addHeader(k, v)
	}
}

type responseHeadersListStore struct {
	*gtk.ListStore
}

func newResponseHeadersListStore() *responseHeadersListStore {
	listStore, err := gtk.ListStoreNew(glib.TYPE_STRING, glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}
	rhls := &responseHeadersListStore{ListStore: listStore}

	return rhls
}

func (rhls *responseHeadersListStore) addHeader(key, value string) {
	iter := rhls.ListStore.Append()
	rhls.SetValue(iter, 0, key)
	rhls.SetValue(iter, 1, value)
}
