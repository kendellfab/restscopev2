package widgets

import (
	"log"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type FoldersTreeView struct {
	*gtk.TreeView
	listStore *FoldersListStore
}

func NewFoldersTreeView(listStore *FoldersListStore) *FoldersTreeView {
	treeView, err := gtk.TreeViewNew()
	if err != nil {
		panic(err)
	}
	ftv := &FoldersTreeView{TreeView: treeView, listStore: listStore}
	ftv.SetModel(listStore)

	folderRenderer, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	folderColumn, err := gtk.TreeViewColumnNewWithAttribute("Folder", folderRenderer, "text", 0)
	if err != nil {
		panic(err)
	}
	ftv.AppendColumn(folderColumn)

	requestRenderer, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	requestColumn, err := gtk.TreeViewColumnNewWithAttribute("Request", requestRenderer, "text", 1)
	if err != nil {
		panic(err)
	}
	ftv.AppendColumn(requestColumn)

	return ftv
}

type FoldersListStore struct {
	*gtk.TreeStore
}

func newFoldersListStore() *FoldersListStore {
	treeStore, err := gtk.TreeStoreNew(glib.TYPE_STRING, glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}
	fls := &FoldersListStore{TreeStore: treeStore}

	return fls
}

func (fls *FoldersListStore) AddFolder(folder models.Folder) {
	parentIter := fls.Append(nil)
	fls.SetValue(parentIter, 0, folder.Name)
	for _, req := range folder.Requests {
		//childIter := fls.Append()
		childIter := fls.Append(parentIter)
		log.Println("Adding children requests:", req.Name)
		fls.SetValue(childIter, 1, req.Name)
	}
}

func (fls *FoldersListStore) AddRequestToFolder(folder string, request models.StorageRequest) error {
	iter, err := fls.GetIterFromString(folder)
	if err != nil {
		return err
	}

	child := fls.Append(iter)
	fls.SetValue(child, 1, request.Name)
	return nil
}

func (fls *FoldersListStore) UpdateFolderName(name, path string) error {
	iter, err := fls.GetIterFromString(path)
	if err != nil {
		return err
	}
	fls.SetValue(iter, 0, name)
	return nil
}

func (fls *FoldersListStore) UpdateRequestName(name, path string) error {
	iter, err := fls.GetIterFromString(path)
	if err != nil {
		return err
	}
	fls.SetValue(iter, 1, name)
	return nil
}
