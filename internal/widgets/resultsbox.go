package widgets

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image/color"
	"log"
	"math"
	"strconv"
	"strings"

	"gonum.org/v1/plot/vg/draw"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"

	"github.com/atotto/clipboard"
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"github.com/gotk3/gotk3/pango"
	"github.com/hfmrow/gotk3_gtksource/source"
	"gitlab.com/kendellfab/restscope/internal/callbacks"
	"gitlab.com/kendellfab/restscope/internal/models"
	"gitlab.com/kendellfab/restscope/internal/style"
)

const (
	AwaitingRequest = "Awaiting Request"
	RequestTime     = "Request Time"
	BodySize        = "Body Size"
)

type ResultsBox struct {
	*gtk.Box
	historyCallback callbacks.HistoryCallback

	lblCode         *ColorLabel
	lblTiming       *ColorLabel
	lblSize         *ColorLabel
	busySpinner     *gtk.Spinner
	lblUrl          *gtk.Label
	lblCount        *gtk.Label
	historyPopover  *gtk.Popover
	displayNotebook *gtk.Notebook
	responseHeaders *ResponseHeadersTreeView

	svResponse            *source.SourceView
	sourceBuffer          *source.SourceBuffer
	sourceLanguageManager *source.SourceLanguageManager
	searchSettings        *source.SourceSearchSettings
	searchContext         *source.SourceSearchContext
	searchEntry           *gtk.Entry

	imageWrapper *gtk.ScrolledWindow
	histogram    *gtk.Image
}

func NewResultsBox(historyCallback callbacks.HistoryCallback) *ResultsBox {
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	if err != nil {
		panic(err)
	}
	rb := &ResultsBox{Box: box, historyCallback: historyCallback}
	rb.SetMarginStart(10)
	rb.SetMarginEnd(10)

	hudBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		panic(err)
	}
	rb.lblCode = NewColorLabel(AwaitingRequest, style.COLOR_YELLOW)
	hudBox.PackStart(rb.lblCode, false, false, 0)

	rb.lblTiming = NewColorLabel(RequestTime, style.COLOR_GRAY)
	rb.lblTiming.SetMarginStart(5)
	hudBox.PackStart(rb.lblTiming, false, false, 0)

	rb.lblSize = NewColorLabel(BodySize, style.COLOR_GRAY)
	rb.lblSize.SetMarginStart(5)
	hudBox.PackStart(rb.lblSize, false, false, 0)

	rb.busySpinner, err = gtk.SpinnerNew()
	if err != nil {
		panic(err)
	}
	rb.busySpinner.SetHAlign(gtk.ALIGN_END)
	hudBox.PackStart(rb.busySpinner, false, false, 0)

	rb.lblUrl, err = gtk.LabelNew("")
	rb.lblUrl.SetMarginStart(5)
	rb.lblUrl.SetEllipsize(pango.ELLIPSIZE_END)
	hudBox.PackStart(rb.lblUrl, false, false, 0)

	historyButton, err := gtk.ButtonNewFromIconName("appointment-symbolic", gtk.ICON_SIZE_MENU)
	if err != nil {
		panic(err)
	}
	hudBox.PackEnd(historyButton, false, false, 0)
	historyButton.Connect("clicked", func() {
		rb.historyPopover, err = gtk.PopoverNew(historyButton)
		if err != nil {
			log.Println("error showing history popover", err)
			return
		}
		rb.historyPopover.Add(newHistoryBox(rb.historyCallback))
		rb.historyPopover.ShowAll()
	})

	ccBtn, _ := gtk.ButtonNewFromIconName("edit-copy", gtk.ICON_SIZE_MENU)
	ccBtn.SetTooltipText("Copy the request url to clipboard.")
	hudBox.PackEnd(ccBtn, false, false, 0)
	ccBtn.Connect("clicked", func() {
		log.Println("Waiting to handle copy")
		url, err := rb.lblUrl.GetText()
		if err == nil {
			err = clipboard.WriteAll(url)
			if err != nil {
				log.Println("error copying text", err)
			}
		}
	})

	projeSep, err := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	if err != nil {
		panic(err)
	}
	projeSep.SetMarginTop(10)
	projeSep.SetMarginBottom(10)

	rb.displayNotebook, err = gtk.NotebookNew()
	if err != nil {
		panic(err)
	}
	rb.displayNotebook.SetVExpand(true)
	rb.displayNotebook.SetHExpand(true)

	rb.responseHeaders = newResponseHeadersTreeView()

	rb.svResponse, err = source.SourceViewNew()
	if err != nil {
		panic(err)
	}
	rb.svResponse.SetShowLineNumbers(true)
	rb.svResponse.SetHExpand(true)
	rb.svResponse.SetVExpand(true)
	rb.svResponse.SetHighlightCurrentLine(true)

	rb.sourceBuffer, err = rb.svResponse.GetBuffer()
	if err != nil {
		panic(err)
	}
	rb.sourceLanguageManager, err = source.SourceLanguageManagerGetDefault()
	if err != nil {
		panic(err)
	}
	rb.searchSettings, err = source.SourceSearchSettingsNew()
	if err != nil {
		panic(err)
	}
	rb.searchContext, err = source.SourceSearchContextNew(rb.sourceBuffer, rb.searchSettings)
	if err != nil {
		panic(err)
	}

	bodyBox, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	searchBox, _ := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	searchBox.SetMarginBottom(5)
	searchBox.SetMarginTop(5)
	searchBox.SetMarginStart(5)
	searchBox.SetMarginEnd(5)
	rb.searchEntry, _ = gtk.EntryNew()
	rb.searchEntry.SetPlaceholderText("Search")
	rb.searchEntry.Connect("changed", rb.searchChanged)
	searchBox.PackStart(rb.searchEntry, false, false, 0)
	rb.lblCount, _ = gtk.LabelNew("")
	searchBox.PackStart(rb.lblCount, false, false, 0)

	scrolledWindow, err := gtk.ScrolledWindowNew(nil, nil)
	if err != nil {
		panic(err)
	}
	scrolledWindow.Add(rb.svResponse)
	bodyBox.Add(searchBox)
	bodyBox.Add(scrolledWindow)

	lblBody, err := gtk.LabelNew("Body")
	if err != nil {
		panic(err)
	}
	rb.displayNotebook.AppendPage(bodyBox, lblBody)

	lblHeaders, err := gtk.LabelNew("Headers")
	if err != nil {
		panic(err)
	}
	rb.displayNotebook.AppendPage(rb.responseHeaders, lblHeaders)

	rb.imageWrapper, err = gtk.ScrolledWindowNew(nil, nil)
	if err != nil {
		panic(err)
	}
	rb.histogram, err = gtk.ImageNew()
	if err != nil {
		panic(err)
	}
	//rb.imageWrapper.SetVExpand(true)
	//rb.imageWrapper.SetHExpand(true)
	//rb.imageWrapper.SetVAlign(gtk.ALIGN_CENTER)
	//rb.imageWrapper.SetHAlign(gtk.ALIGN_CENTER)
	rb.imageWrapper.Add(rb.histogram)

	lblCharts, err := gtk.LabelNew("History")
	if err != nil {
		log.Fatal(err)
	}
	rb.displayNotebook.AppendPage(rb.imageWrapper, lblCharts)

	rb.PackStart(hudBox, false, false, 0)
	rb.PackStart(projeSep, false, true, 0)
	rb.PackStart(rb.displayNotebook, true, true, 0)

	return rb
}

func (rb *ResultsBox) searchChanged(editable *gtk.Entry) {
	searchText, err := rb.searchEntry.GetText()
	if err != nil {
		log.Println("error getting search text", err)
		return
	}

	rb.searchSettings.SetSearchText(searchText)
	startIter := rb.sourceBuffer.GetIterAtOffset(0)

	if len(searchText) == 0 {
		rb.lblCount.SetText("")
		return
	}

	rb.searchContext.ForwardAsync(startIter, nil, func(obj *glib.Object, res *glib.AsyncResult) {
		glib.IdleAdd(rb.searchComplete)
	})
}

func (rb *ResultsBox) searchComplete() {
	resCount := rb.searchContext.GetOccurencesCount()
	log.Println("Occurenced ->", resCount)
	if resCount > 0 {
		rb.lblCount.SetText(strconv.Itoa(resCount))
	}
}

func (rb *ResultsBox) UpdateScheme(scheme *source.SourceStyleScheme) {
	rb.sourceBuffer.SetStyleSheme(scheme)
}

func (rb *ResultsBox) SetCodeLabelText(text string) {
	rb.lblCode.SetText(text)
}

func (rb *ResultsBox) SetBusySpinner(isBusy bool) {
	if isBusy {
		rb.busySpinner.Start()
	} else {
		rb.busySpinner.Stop()
	}
}

func (rb *ResultsBox) Reset() {
	rb.lblCode.SetText(AwaitingRequest)
	rb.lblTiming.SetText(RequestTime)
	rb.lblSize.SetText(BodySize)
	rb.lblUrl.SetText("")
	rb.responseHeaders.reset()
	rb.sourceBuffer.SetText("")
}

func (rb *ResultsBox) UpdateForResponse(rs models.GuiResponse, rq *models.StorageRequest) {
	rb.SetBusySpinner(false)
	if rs.ErrorMessage != "" {
		rb.lblCode.SetText("Error")
		rb.lblCode.SetColor(style.COLOR_RED)
		rb.sourceBuffer.SetText(rs.ErrorMessage)
		slm, err := source.SourceLanguageManagerGetDefault()
		if err == nil {
			if lang, err := slm.GetGuessLanguage("", "text/plain"); err == nil {
				rb.sourceBuffer.SetLanguage(lang)
			} else {
				log.Println("error getting the language", err)
			}
		}
		return
	}

	rb.lblCode.SetText(fmt.Sprintf("%d", rs.Code))

	if rs.Code >= 200 && rs.Code < 300 {
		rb.lblCode.SetColor(style.COLOR_GREEN)
	} else if rs.Code >= 300 && rs.Code < 400 {
		rb.lblCode.SetColor(style.COLOR_BLUE)
	} else if rs.Code >= 400 && rs.Code < 500 {
		rb.lblCode.SetColor(style.COLOR_ORANGE)
	} else {
		rb.lblCode.SetColor(style.COLOR_RED)
	}
	rb.lblUrl.SetText(rs.URL)

	keyContentType := "content-type"
	contentType := "application/octet-stream"
	if ct, ok := rs.Headers[keyContentType]; ok {
		parts := strings.Split(ct, "; ")
		contentType = parts[0]
	}

	rb.lblTiming.SetText(fmt.Sprintf("%d ms", rs.RecvTime))
	rb.lblSize.SetText(byteCountBinary(rs.Size))

	rb.responseHeaders.addHeaders(rs.Headers)

	var tempJson map[string]interface{}
	err := json.Unmarshal([]byte(rs.Body), &tempJson)
	if err == nil {
		if res, marshErr := json.MarshalIndent(tempJson, "", "    "); marshErr == nil {
			rs.Body = string(res)
			contentType = "application/json"
		}
	}

	rb.sourceBuffer.SetText(rs.Body)

	if lang, langErr := rb.sourceLanguageManager.GetGuessLanguage("", contentType); langErr == nil {
		rb.sourceBuffer.SetLanguage(lang)
		rb.sourceBuffer.SetHighlightSyntax(true)
	}

	rb.generateHistogram(rq)
}

func (rb *ResultsBox) NotifyResize(rq *models.StorageRequest) {
	rb.generateHistogram(rq)
}

func (rb *ResultsBox) generateHistogram(rq *models.StorageRequest) {
	responses, err := rb.historyCallback.ResponsesForRequestReverse(25)
	if len(responses) == 0 {
		return
	}
	if err == nil {
		go func() {

			var grpRec plotter.Values
			var grpFirst plotter.Values
			var grpConnect plotter.Values
			var grpDone plotter.Values
			var grpStart plotter.Values

			var names []string

			for i := len(responses) - 1; i >= 0; i-- {
				response := responses[i]
				grpRec = append(grpRec, float64(response.RecvTime-response.FirstByte))
				grpFirst = append(grpFirst, float64(response.FirstByte-response.ConnectTime))
				grpConnect = append(grpConnect, float64(response.ConnectTime-response.DnsDone))
				grpDone = append(grpDone, float64(response.DnsDone-response.DnsStart))
				grpStart = append(grpStart, float64(response.DnsStart))
				names = append(names, response.At.Format("01/02/06@15:04:05"))
			}

			colorWhite := color.RGBA{R: 255, G: 255, B: 255, A: 255}

			p := plot.New()
			grid := plotter.NewGrid()
			grid.Vertical.Width = vg.Points(0)
			p.Add(grid)
			p.Title.Text = rq.Name
			p.Title.TextStyle.Color = colorWhite
			p.Title.Padding = vg.Points(5)
			p.Y.Label.Text = "Overall Duration (ms)"
			p.Y.Label.TextStyle.Color = colorWhite
			p.Y.Tick.Color = colorWhite
			p.Y.Tick.Label.Color = colorWhite
			p.Y.LineStyle.Color = colorWhite
			p.Y.Padding = vg.Points(1)

			p.X.Tick.Label.Rotation = math.Pi / 2.5
			p.X.Tick.Label.YAlign = draw.YCenter
			p.X.Tick.Label.XAlign = draw.XRight
			p.X.Tick.Label.Color = colorWhite
			p.X.Padding = vg.Points(5)
			p.X.Tick.Color = colorWhite
			p.BackgroundColor = color.RGBA{R: 50, G: 50, B: 50, A: 255}

			w := vg.Points(18)

			barsRecv, err := plotter.NewBarChart(grpRec, w)
			if err != nil {
				panic(err)
			}
			barsRecv.LineStyle.Width = vg.Length(0)
			barsRecv.Color = plotutil.Color(0)

			barsFirst, err := plotter.NewBarChart(grpFirst, w)
			if err != nil {
				panic(err)
			}
			barsFirst.LineStyle.Width = vg.Length(0)
			barsFirst.Color = plotutil.Color(1)

			barsConnect, err := plotter.NewBarChart(grpConnect, w)
			if err != nil {
				panic(err)
			}
			barsConnect.LineStyle.Width = vg.Length(0)
			barsConnect.Color = plotutil.Color(2)

			barsDone, err := plotter.NewBarChart(grpDone, w)
			if err != nil {
				panic(err)
			}
			barsDone.LineStyle.Width = vg.Length(0)
			barsDone.Color = plotutil.Color(3)

			barsStart, err := plotter.NewBarChart(grpStart, w)
			if err != nil {
				panic(err)
			}
			barsStart.LineStyle.Width = vg.Length(0)
			barsStart.Color = plotutil.Color(4)

			barsRecv.StackOn(barsFirst)
			barsFirst.StackOn(barsConnect)
			barsConnect.StackOn(barsDone)
			barsDone.StackOn(barsStart)

			p.Add(barsRecv, barsFirst, barsConnect, barsDone, barsStart)
			p.Legend.Add("Dns Start", barsStart)
			p.Legend.Add("Dns Done", barsDone)
			p.Legend.Add("Connect Time", barsConnect)
			p.Legend.Add("First Byte", barsFirst)
			p.Legend.Add("Recv Time", barsRecv)
			p.Legend.TextStyle.Color = colorWhite
			p.Legend.Top = true
			p.Legend.Padding = vg.Points(5)
			p.Legend.XOffs = vg.Points(-10)
			p.NominalX(names...)

			if historyLoader, err := gdk.PixbufLoaderNew(); err == nil {
				var buf bytes.Buffer

				// Multiplying by 0.75 to convert pixels to points
				outWidth := float64(rb.imageWrapper.GetAllocatedWidth()) * 0.75
				outHeight := float64(rb.imageWrapper.GetAllocatedHeight()) * 0.75

				wr, _ := p.WriterTo(vg.Points(float64(outWidth)), vg.Points(float64(outHeight)), "png")
				wr.WriteTo(&buf)
				historyPixBuf, _ := historyLoader.WriteAndReturnPixbuf(buf.Bytes())
				glib.IdleAdd(func() {
					rb.setImage(historyPixBuf)
				})
			}

		}()
	} else {
		log.Println("error loading responses for request", err)
	}
}

func (rb *ResultsBox) setImage(pixbuf *gdk.Pixbuf) {
	rb.histogram.SetFromPixbuf(pixbuf)
}

func byteCountDecimal(b int64) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %cB", float64(b)/float64(div), "kMGTPE"[exp])
}

func byteCountBinary(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %ciB", float64(b)/float64(div), "KMGTPE"[exp])
}
