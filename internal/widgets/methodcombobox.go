package widgets

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"log"
)

var methods = []string{"GET", "POST", "PUT", "DELETE"}

type MethodComboBox struct {
	*gtk.ComboBox
}

func NewMethodComboBox() *MethodComboBox {

	listStore, err := gtk.ListStoreNew(glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}


	for _, method := range methods {
		iter := listStore.Append()
		err := listStore.SetValue(iter, 0, method)
		if err != nil {
			log.Fatal(err)
		}
	}

	cell, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}


	mcb := &MethodComboBox{}
	mcb.ComboBox, err = gtk.ComboBoxNew()
	if err != nil {
		panic(err)
	}

	mcb.PackStart(cell, true)
	mcb.AddAttribute(cell, "text", 0)
	mcb.SetModel(listStore)
	mcb.SetActive(0)

	return mcb
}

func (mcb *MethodComboBox) GetMethod() string {
	active := mcb.GetActive()
	return methods[active]
}

func (mcb *MethodComboBox) UpdateActive(input string) {
	for i, method := range methods {
		if input == method {
			mcb.SetActive(i)
			break
		}
	}
}