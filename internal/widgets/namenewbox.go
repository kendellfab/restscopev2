package widgets

import "github.com/gotk3/gotk3/gtk"

type NameNewBox struct {
	*gtk.Grid
	nameLabel *gtk.Label
	nameEntry *gtk.Entry
	doAddName func(name string)
}

func newNameNewBox(doAddName func(name string), labelText string) *NameNewBox {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}
	nnb := &NameNewBox{Grid: grid, doAddName: doAddName}
	nnb.SetMarginStart(10)
	nnb.SetMarginTop(10)
	nnb.SetMarginEnd(10)
	nnb.SetMarginBottom(10)

	nnb.nameLabel, err = gtk.LabelNew(labelText)
	if err != nil {
		panic(err)
	}
	nnb.nameEntry, err = gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	nnb.nameEntry.SetHExpand(true)

	nnb.SetRowSpacing(10)
	nnb.SetColumnSpacing(5)

	nnb.Attach(nnb.nameLabel, 0, 0, 1, 1)
	nnb.Attach(nnb.nameEntry, 1, 0, 1, 1)

	doAdd, err := gtk.ButtonNewWithLabel("Add")
	if err != nil {
		panic(err)
	}
	nnb.Attach(doAdd, 0, 1, 2, 1)

	doAdd.Connect("clicked", func() {
		res, _ := nnb.nameEntry.GetText()
		nnb.doAddName(res)
		nnb.nameEntry.SetText("")
	})

	return nnb
}
