package widgets

import (
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/callbacks"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type ProjectBox struct {
	*gtk.Box
	guiRequestUpdateCallback  callbacks.GuiRequestUpdateCallback
	projectManagementCallback callbacks.ProjectManagementCallback
	availableProjects         []models.Project
	projectsListStore         *ProjectsListStore
	projectsComboBox          *ProjectsComboBox
	addProjectButton          *gtk.Button
	editProjectButton         *gtk.Button
	projectAddPopover         *gtk.Popover
	addFolderButton           *gtk.Button
	editProjectPopover        *gtk.Popover
	selectedProject           models.Project
	projectActionBox          *ProjectActionBox
	folderAddPopover          *gtk.Popover
	foldersListStore          *FoldersListStore
	foldersTreeView           *FoldersTreeView
	folders                   []models.Folder
	requestActionPopover      *gtk.Popover
	folderActionPopover       *gtk.Popover
	selectedFolder            models.Folder
}

func NewProjectBox(guiRequestUpdateCallback callbacks.GuiRequestUpdateCallback, projectManagementCallback callbacks.ProjectManagementCallback) *ProjectBox {
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	if err != nil {
		panic(err)
	}
	pb := &ProjectBox{Box: box, guiRequestUpdateCallback: guiRequestUpdateCallback, projectManagementCallback: projectManagementCallback}
	pb.SetMarginEnd(10)
	projectListBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		panic(err)
	}
	pb.projectsListStore = newProjectsListStore()
	pb.projectsComboBox = newProjectsComboBox(pb.projectsListStore)
	pb.projectsComboBox.SetHExpand(true)
	pb.projectsComboBox.SetSizeRequest(200, 0)
	pb.projectsComboBox.Connect("changed", pb.projectChanged)
	pb.addProjectButton, err = gtk.ButtonNewFromIconName("list-add", gtk.ICON_SIZE_MENU)
	if err != nil {
		panic(err)
	}
	pb.addProjectButton.SetSensitive(false)
	pb.editProjectButton, err = gtk.ButtonNewFromIconName("document-edit", gtk.ICON_SIZE_MENU)
	if err != nil {
		panic(err)
	}
	pb.editProjectButton.SetSensitive(false)
	projectListBox.Add(pb.projectsComboBox)
	projectListBox.Add(pb.addProjectButton)
	projectListBox.Add(pb.editProjectButton)
	pb.Add(projectListBox)

	pb.projectAddPopover, err = gtk.PopoverNew(pb.addProjectButton)
	if err != nil {
		panic(err)
	}
	pb.projectAddPopover.Add(newNameNewBox(pb.doAddProjectName, "Project Name"))
	pb.addProjectButton.Connect("clicked", func() {
		pb.projectAddPopover.ShowAll()
	})

	pb.editProjectPopover, err = gtk.PopoverNew(pb.editProjectButton)
	if err != nil {
		panic(err)
	}
	pb.projectActionBox = newProjectActionBox(pb.doEditProjectName)
	pb.editProjectPopover.Add(pb.projectActionBox)
	pb.editProjectButton.Connect("clicked", func() {
		pb.projectActionBox.setProjectName(pb.selectedProject.Name)
		pb.editProjectPopover.ShowAll()
	})

	projSep, err := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	if err != nil {
		panic(err)
	}
	projSep.SetMarginTop(10)
	projSep.SetMarginBottom(10)
	pb.Add(projSep)

	auxBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		panic(err)
	}
	pb.addFolderButton, err = gtk.ButtonNewFromIconName("folder-new", gtk.ICON_SIZE_MENU)
	if err != nil {
		panic(err)
	}
	pb.addFolderButton.SetTooltipText("Click the add button to add a new folder.  Select the new folder in the list to add a request.")
	pb.addFolderButton.SetSensitive(false)

	pb.folderAddPopover, err = gtk.PopoverNew(pb.addFolderButton)
	if err != nil {
		panic(err)
	}
	pb.folderAddPopover.Add(newNameNewBox(pb.doAddFolderName, "Folder name"))
	pb.addFolderButton.Connect("clicked", func() {
		pb.folderAddPopover.ShowAll()
	})

	auxBox.PackStart(pb.addFolderButton, false, false, 0)

	pb.Add(auxBox)

	pb.foldersListStore = newFoldersListStore()
	pb.foldersTreeView = NewFoldersTreeView(pb.foldersListStore)
	pb.foldersTreeView.SetShowExpanders(true)
	pb.foldersTreeView.SetActivateOnSingleClick(true)
	pb.foldersTreeView.Connect("button-press-event", pb.rightClick)

	pb.foldersTreeView.Connect("row-activated", pb.rowActivated)

	foldersScroll, err := gtk.ScrolledWindowNew(nil, nil)
	if err != nil {
		panic(err)
	}
	foldersScroll.SetVExpand(true)
	foldersScroll.Add(pb.foldersTreeView)
	pb.Add(foldersScroll)

	return pb
}

func (pb *ProjectBox) rightClick(treeView *gtk.TreeView, event *gdk.Event) {
	eventButton := gdk.EventButtonNewFromEvent(event)
	if eventButton.Button() == gdk.BUTTON_SECONDARY {
		path, column, _, _, _ := pb.foldersTreeView.GetPathAtPos(int(eventButton.X()), int(eventButton.Y()))
		if path.GetDepth() == 2 {
			rcRequest := pb.folders[path.GetIndices()[0]].Requests[path.GetIndices()[1]]

			rect := pb.foldersTreeView.GetCellArea(path, column)
			rect.SetY(rect.GetY() + 35)
			pb.requestActionPopover, _ = gtk.PopoverNew(pb.foldersTreeView)
			pb.requestActionPopover.SetPointingTo(*rect)
			pb.requestActionPopover.Add(NewRequestActionBox(rcRequest, func(name string) {
				pb.requestActionPopover.Hide()
				rcRequest.Name = name
				pb.projectManagementCallback.DoUpdateRequest(rcRequest)
				pb.foldersListStore.UpdateRequestName(name, path.String())
			}, func(request models.StorageRequest) {
				pb.requestActionPopover.Hide()
				pb.projectManagementCallback.DoRequestDelete(rcRequest, pb.selectedProject.ID)
			}))
			pb.requestActionPopover.ShowAll()
		}
	}
}

func (pb *ProjectBox) rowActivated(treeView *gtk.TreeView, path *gtk.TreePath, column *gtk.TreeViewColumn) {
	if path.GetDepth() == 1 {
		rowPath := path.String()
		pb.selectedFolder = pb.folders[path.GetIndices()[0]]

		rect := treeView.GetCellArea(path, column)
		rect.SetY(rect.GetY() + 20)
		pb.folderActionPopover, _ = gtk.PopoverNew(treeView)
		pb.folderActionPopover.SetPointingTo(*rect)
		pb.folderActionPopover.Add(NewFoldersActionBox(func(name string) {
			pb.folderActionPopover.Hide()
			request := models.StorageRequest{ID: 0, Name: name, FolderID: pb.selectedFolder.ID, GuiRequest: &models.GuiRequest{}}
			reqId := pb.projectManagementCallback.DoAddRequest(request, pb.selectedProject.ID)
			request.ID = reqId
			pb.selectedFolder.Requests = append(pb.selectedFolder.Requests, request)
			// We need to update the folder that is in the array of folders.
			for fIDX, _ := range pb.folders {
				if pb.folders[fIDX].ID == pb.selectedFolder.ID {
					pb.folders[fIDX] = pb.selectedFolder
				}
			}
			pb.foldersListStore.AddRequestToFolder(rowPath, request)
		}, pb.handleDeleteFolderRequest, func(folderName string) {
			pb.selectedFolder.Name = folderName
			if pb.projectManagementCallback.DoUpdateFolder(pb.selectedFolder) {
				pb.folderActionPopover.Hide()
				pb.foldersListStore.UpdateFolderName(folderName, rowPath)
			}
		}, "New Request Name", "New Folder Name", pb.selectedFolder.Name))
		pb.folderActionPopover.ShowAll()
	} else {
		selectedRequest := pb.folders[path.GetIndices()[0]].Requests[path.GetIndices()[1]]
		pb.projectManagementCallback.SetRequestSelected(selectedRequest)
	}
}

func (pb *ProjectBox) projectChanged() {
	active := pb.projectsComboBox.GetActive()
	if active >= 0 {
		pb.selectedProject = pb.availableProjects[active]
		pb.projectManagementCallback.SetProjectSelected(pb.selectedProject)
	}
}

func (pb *ProjectBox) doAddProjectName(name string) {
	project := models.Project{ID: 0, Name: name}
	id := pb.projectManagementCallback.DoAddProject(project)
	pb.projectAddPopover.Hide()
	project.ID = id
	pb.projectsListStore.AddProject(project)
	pb.availableProjects = append(pb.availableProjects, project)
	pb.projectsComboBox.SetActive(len(pb.availableProjects) - 1)
	pb.enableButtons()
}

func (pb *ProjectBox) doEditProjectName(name string) {
	pb.selectedProject.Name = name
	if pb.projectManagementCallback.DoUpdateProject(pb.selectedProject) {
		pb.editProjectPopover.Hide()
		pb.projectsComboBox.UpdateActiveName(name)
	}
}

func (pb *ProjectBox) enableButtons() {
	pb.addProjectButton.SetSensitive(true)
	if pb.availableProjects != nil && len(pb.availableProjects) > 0 {
		pb.editProjectButton.SetSensitive(true)
		pb.addFolderButton.SetSensitive(true)
	} else {
		pb.addFolderButton.SetSensitive(false)
	}
}

func (pb *ProjectBox) SetProjects(projects []models.Project) {
	pb.projectsListStore.Clear()
	pb.foldersListStore.Clear()
	pb.availableProjects = projects

	if len(projects) > 0 {
		for _, proj := range projects {
			pb.projectsListStore.AddProject(proj)
		}
		pb.projectsComboBox.SetActive(0)
	}

	pb.enableButtons()
}

func (pb *ProjectBox) doAddFolderName(name string) {
	folder := models.Folder{ID: 0, Name: name, ProjectID: pb.selectedProject.ID}
	id := pb.projectManagementCallback.DoAddFolder(folder)
	folder.ID = id
	pb.foldersListStore.AddFolder(folder)
	pb.folders = append(pb.folders, folder)
	pb.folderAddPopover.Hide()
}

func (pb *ProjectBox) SetFolders(folders []models.Folder) {
	pb.foldersListStore.Clear()
	pb.folders = folders
	for _, folder := range folders {
		pb.foldersListStore.AddFolder(folder)
	}
}

func (pb *ProjectBox) handleDeleteFolderRequest() {
	pb.projectManagementCallback.DoFolderDelete(pb.selectedFolder, pb.selectedProject.ID)
	pb.folderActionPopover.Hide()
}
