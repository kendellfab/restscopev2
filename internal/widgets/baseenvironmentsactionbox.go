package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type BaseEnvironmentsActionBox struct {
	*gtk.Grid
}

func NewBaseEnvironmentsActionBox(selectedEnv models.Environment, doDeleteEnv func(env models.Environment), doEditEnv func(env models.Environment, name string), doAddChild func(env models.Environment, childName string)) *BaseEnvironmentsActionBox {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}
	beab := &BaseEnvironmentsActionBox{Grid: grid}
	beab.SetMarginStart(10)
	beab.SetMarginTop(10)
	beab.SetMarginEnd(10)
	beab.SetMarginBottom(10)
	beab.SetRowSpacing(15)
	beab.SetColumnSpacing(5)

	childLabel, _ := gtk.LabelNew("Add Child Environment")
	childEntry, _ := gtk.EntryNew()
	childEntry.SetPlaceholderText("Child Name")
	doAddChildBtn, _ := gtk.ButtonNewWithLabel("Add Child")
	doAddChildBtn.Connect("clicked", func() {
		name, _ := childEntry.GetText()
		doAddChild(selectedEnv, name)
	})
	beab.Attach(childLabel, 0, 0, 1, 1)
	beab.Attach(childEntry, 1, 0, 1, 1)
	beab.Attach(doAddChildBtn, 0, 1, 2, 1)

	hSep, _ := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	beab.Attach(hSep, 0, 2, 2, 1)

	editLabel, _ := gtk.LabelNew("New Environment Name")
	editEntry, _ := gtk.EntryNew()
	editEntry.SetPlaceholderText(selectedEnv.Name)
	doEditBtn, _ := gtk.ButtonNewWithLabel("Edit Environment Name")
	doEditBtn.Connect("clicked", func() {
		name, _ := editEntry.GetText()
		doEditEnv(selectedEnv, name)
	})
	beab.Attach(editLabel, 0, 3, 1, 1)
	beab.Attach(editEntry, 1, 3, 1, 1)
	beab.Attach(doEditBtn, 0, 4, 2, 1)

	editSep, _ := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	beab.Attach(editSep, 0, 5, 2, 1)

	doDeleteBtn, _ := gtk.ButtonNewWithLabel("Delete")
	doDeleteBtn.Connect("clicked", func() {
		doDeleteEnv(selectedEnv)
	})

	beab.Attach(doDeleteBtn, 0, 6, 2, 1)

	return beab
}
