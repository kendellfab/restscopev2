package widgets

import (
	"github.com/gotk3/gotk3/gtk"
)

type ColorLabel struct {
	*gtk.EventBox
	label *gtk.Label
}

func NewColorLabel(text, color string) *ColorLabel {
	eventBox, err := gtk.EventBoxNew()
	if err != nil {
		panic(err)
	}
	cl := &ColorLabel{EventBox: eventBox}

	label, err := gtk.LabelNew(text)
	if err != nil {
		panic(err)
	}
	cl.Add(label)
	cl.label = label

	cl.label.SetMarginTop(10)
	cl.label.SetMarginBottom(10)
	cl.label.SetMarginStart(12)
	cl.label.SetMarginEnd(12)

	cl.SetName(color)

	return cl
}

func (cl *ColorLabel) SetText(text string) {
	cl.label.SetText(text)
}

func (cl *ColorLabel) SetColor(color string) {
	cl.SetName(color)
}
