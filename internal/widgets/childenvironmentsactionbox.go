package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type ChildEnvironmentsActionBox struct {
	*gtk.Grid
}

func NewChildEnvironmentsActionBox(selectedEnv models.Environment, doDeleteEnv func(env models.Environment), doEditEnv func(env models.Environment, name string)) *ChildEnvironmentsActionBox {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}
	ceab := &ChildEnvironmentsActionBox{Grid: grid}
	ceab.SetMarginStart(10)
	ceab.SetMarginTop(10)
	ceab.SetMarginEnd(10)
	ceab.SetMarginBottom(10)
	ceab.SetRowSpacing(15)
	ceab.SetColumnSpacing(5)

	editLabel, _ := gtk.LabelNew("New Environment Name")
	editEntry, _ := gtk.EntryNew()
	editEntry.SetPlaceholderText(selectedEnv.Name)
	doEditBtn, _ := gtk.ButtonNewWithLabel("Edit Environment Name")
	doEditBtn.Connect("clicked", func() {
		name, _ := editEntry.GetText()
		doEditEnv(selectedEnv, name)
	})
	ceab.Attach(editLabel, 0, 0, 1, 1)
	ceab.Attach(editEntry, 1, 0, 1, 1)
	ceab.Attach(doEditBtn, 0, 1, 2, 1)

	hSep, _ := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	ceab.Attach(hSep, 0, 2, 2, 1)

	doDeleteBtn, _ := gtk.ButtonNewWithLabel("Delete")
	doDeleteBtn.Connect("clicked", func() {
		doDeleteEnv(selectedEnv)
	})

	ceab.Attach(doDeleteBtn, 0, 3, 2, 1)

	return ceab
}
