package widgets

import "github.com/gotk3/gotk3/gtk"

type FoldersActionBox struct {
	*gtk.Grid
}

func NewFoldersActionBox(doAddName func(name string), requestDelete func(), doEditFolder func(folderName string), labelText, editTitle, folderName string) *FoldersActionBox {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}

	fab := &FoldersActionBox{Grid: grid}
	fab.SetMarginTop(10)
	fab.SetMarginStart(10)
	fab.SetMarginEnd(10)
	fab.SetMarginBottom(10)
	fab.SetRowSpacing(10)
	fab.SetColumnSpacing(5)

	nameLabel, err := gtk.LabelNew(labelText)
	if err != nil {
		panic(err)
	}
	nameEntry, err := gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	fab.Attach(nameLabel, 0, 0, 1, 1)
	fab.Attach(nameEntry, 1, 0, 1, 1)

	doAdd, err := gtk.ButtonNewWithLabel("Add Request")
	if err != nil {
		panic(err)
	}
	fab.Attach(doAdd, 0, 1, 2, 1)
	doAdd.Connect("clicked", func() {
		name, _ := nameEntry.GetText()
		doAddName(name)
		nameEntry.SetText("")
	})

	hSep, err := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	if err != nil {
		panic(err)
	}
	hSep.SetMarginTop(10)
	hSep.SetMarginBottom(10)
	fab.Attach(hSep, 0, 2, 2, 1)

	editLabel, _ := gtk.LabelNew(editTitle)
	editEntry, _ := gtk.EntryNew()
	editEntry.SetPlaceholderText(folderName)
	editEntry.SetHExpand(true)
	doEditButton, _ := gtk.ButtonNewWithLabel("Edit name")
	doEditButton.Connect("clicked", func() {
		edit, _ := editEntry.GetText()
		doEditFolder(edit)
		editEntry.SetText("")
	})

	fab.Attach(editLabel, 0, 3, 1, 1)
	fab.Attach(editEntry, 1, 3, 1, 1)
	fab.Attach(doEditButton, 0, 4, 2, 1)

	secondSep, _ := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	secondSep.SetMarginTop(10)
	secondSep.SetMarginBottom(10)
	fab.Attach(secondSep, 0, 5, 2, 1)

	delBtn, _ := gtk.ButtonNewWithLabel("Delete")
	fab.Attach(delBtn, 0, 6, 2, 1)
	delBtn.Connect("clicked", func() {
		requestDelete()
	})

	return fab
}
