package widgets

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

var authTypes = []string{"None", "Basic", "Bearer"}

type AuthTypeComboBox struct {
	*gtk.ComboBox
}

func NewAuthTypeComboBox() *AuthTypeComboBox {
	combobox, err := gtk.ComboBoxNew()
	if err != nil {
		panic(err)
	}
	atcm := &AuthTypeComboBox{ComboBox: combobox}

	listStore := NewAuthTypeListStore()
	atcm.SetModel(listStore)
	cell, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	atcm.PackStart(cell, true)
	atcm.AddAttribute(cell, "text", 0)
	atcm.SetActive(0)

	return atcm
}

type AuthTypeListStore struct {
	*gtk.ListStore
}

func NewAuthTypeListStore() *AuthTypeListStore {
	listStore, err := gtk.ListStoreNew(glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}
	atls := &AuthTypeListStore{ListStore: listStore}

	for _, at := range authTypes {
		iter := atls.Append()
		atls.SetValue(iter, 0, at)
	}

	return atls
}
