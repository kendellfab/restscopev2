package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/callbacks"
	"gitlab.com/kendellfab/restscope/internal/models"
	"gitlab.com/kendellfab/restscope/internal/style"
)

type HistoryBox struct {
	*gtk.Box
	listBox         *gtk.ListBox
	historyCallback callbacks.HistoryCallback
}

func newHistoryBox(historyCallback callbacks.HistoryCallback) *HistoryBox {
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	if err != nil {
		panic(err)
	}
	hb := &HistoryBox{Box: box, historyCallback: historyCallback}
	hb.SetMarginStart(10)
	hb.SetMarginTop(10)
	hb.SetMarginEnd(10)
	hb.SetMarginBottom(10)

	clearAllButton, err := gtk.ButtonNewWithLabel("Clear All")
	if err != nil {
		panic(err)
	}
	clearAllButton.Connect("clicked", func() {
		if historyCallback.ClearAllResponsesForRequest() == nil {
			for {
				row := hb.listBox.GetRowAtIndex(0)
				if row == nil {
					break
				}
				row.Destroy()
			}
		}
	})
	hb.PackStart(clearAllButton, false, false, 0)
	clearAllButton.SetMarginBottom(4)

	sep, err := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	if err != nil {
		panic(err)
	}
	hb.PackStart(sep, false, false, 0)

	sw, err := gtk.ScrolledWindowNew(nil, nil)
	if err != nil {
		panic(err)
	}
	sw.SetSizeRequest(400, 600)
	sw.SetMarginTop(4)

	hb.listBox, err = gtk.ListBoxNew()
	if err != nil {
		panic(err)
	}
	noResponses := NewColorLabel("No Responses", style.COLOR_BLUE)
	hb.listBox.SetPlaceholder(noResponses)
	hb.listBox.SetSelectionMode(gtk.SELECTION_SINGLE)
	sw.Add(hb.listBox)

	hb.PackStart(sw, false, false, 0)

	responses, err := hb.historyCallback.ListResponses()
	if err == nil {
		for _, response := range responses {
			hb.listBox.Add(newHistoryListBoxItem(response, hb.removeResponse, hb.historyCallback.ResponseSelected))
		}
		//hb.listBox.Connect("selected-rows-changed", func() {
		//	log.Println("Row Selected")
		//
		//	if respData, err := hb.listBox.GetSelectedRow().GetProperty("response"); err == nil {
		//		if resp, ok := respData.(models.GuiResponse); ok {
		//			hb.historyCallback.ResponseSelected(resp)
		//		}
		//	} else {
		//		log.Println("error getting property", err)
		//	}
		//})
	}

	return hb
}

func (hb *HistoryBox) removeResponse(response models.GuiResponse, item *HistoryListBoxItem) {
	if hb.historyCallback.DeleteResponse(response) == nil {
		hb.listBox.Remove(item)
	}
}
