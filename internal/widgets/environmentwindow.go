package widgets

import (
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/callbacks"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type EnvironmentWindow struct {
	*gtk.Window
	envs                  []models.Environment
	envManagementCallback callbacks.EnvironmentManagementCallback
	addEnvPopover         *gtk.Popover
	envTreeStore          *EnvironmentsTreeStore
	envTreeView           *EnvironmentsTreeView
	kvList                *KeyValueList
	selectedEnvironment   models.Environment
	actionPopover         *gtk.Popover
}

func NewEnvironmentWindow(envs []models.Environment, envManagementCallback callbacks.EnvironmentManagementCallback) *EnvironmentWindow {
	window, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		panic(err)
	}
	ew := &EnvironmentWindow{Window: window, envManagementCallback: envManagementCallback}
	ew.SetTitle("Environments")
	ew.SetDefaultSize(960, 600)
	ew.SetDestroyWithParent(true)

	contentPaned, err := gtk.PanedNew(gtk.ORIENTATION_HORIZONTAL)
	if err != nil {
		panic(err)
	}
	contentPaned.SetMarginStart(10)
	contentPaned.SetMarginTop(5)
	contentPaned.SetMarginEnd(10)
	contentPaned.SetMarginBottom(10)

	listWrapper, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	if err != nil {
		panic(err)
	}
	listWrapper.SetMarginEnd(5)

	buttonBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		panic(err)
	}
	buttonBox.SetMarginStart(5)
	buttonBox.SetMarginTop(5)
	buttonBox.SetMarginEnd(5)
	buttonBox.SetMarginBottom(5)
	buttonBox.SetSizeRequest(200, -1)

	addButton, _ := gtk.ButtonNewFromIconName("list-add", gtk.ICON_SIZE_MENU)
	ew.addEnvPopover, _ = gtk.PopoverNew(addButton)
	ew.addEnvPopover.Add(newNameNewBox(ew.addEnvironmentCallback, "Environment Name"))
	addButton.Connect("clicked", func() {
		ew.addEnvPopover.ShowAll()
	})
	buttonBox.Add(addButton)
	listWrapper.Add(buttonBox)
	contentPaned.Pack1(listWrapper, false, false)

	ew.envTreeStore = newEnvironmentsTreeStore()
	ew.envTreeView = NewEnvironmentsTreeView(ew.envTreeStore)
	ew.envTreeView.Connect("row-activated", ew.rowActivated)
	ew.envTreeView.Connect("button-press-event", ew.rowRightClick)
	listWrapper.Add(ew.envTreeView)

	sw, _ := gtk.ScrolledWindowNew(nil, nil)
	sw.SetMarginStart(5)
	sw.SetHExpand(true)
	sw.SetVExpand(true)

	ew.kvList = NewKeyValueList(ew.kvListUpdated, ew.kvListUpdated, ew.kvListUpdated)
	ew.kvList.SetHExpand(true)
	ew.kvList.SetVExpand(true)

	sw.Add(ew.kvList)
	contentPaned.Pack2(sw, true, true)

	ew.Add(contentPaned)
	ew.setEnvironments(envs)
	return ew
}

func (ew *EnvironmentWindow) addEnvironmentCallback(name string) {
	ew.addEnvPopover.Hide()
	var kvp []models.KVPair
	env := models.Environment{ID: 0, Name: name, Pairs: kvp}
	newEnvs, err := ew.envManagementCallback.DoAddEnvironment(env)
	if err != nil {
		return
	}
	ew.setEnvironments(newEnvs)
}

func (ew *EnvironmentWindow) rowActivated(treeView *gtk.TreeView, path *gtk.TreePath, column *gtk.TreeViewColumn) {
	if path.GetDepth() == 1 {
		ew.selectedEnvironment = ew.envs[path.GetIndices()[0]]
		ew.kvList.Reset()
		ew.kvList.UpdatePairs(ew.selectedEnvironment.Pairs)
	} else if path.GetDepth() == 2 {
		ew.selectedEnvironment = ew.envs[path.GetIndices()[0]].Children[path.GetIndices()[1]]
		ew.kvList.Reset()
		ew.kvList.UpdatePairs(ew.selectedEnvironment.Pairs)
	}
}

func (ew *EnvironmentWindow) rowRightClick(treeView *gtk.TreeView, event *gdk.Event) {
	eventButton := gdk.EventButtonNewFromEvent(event)
	if eventButton.Button() == gdk.BUTTON_SECONDARY {
		path, column, _, _, _ := treeView.GetPathAtPos(int(eventButton.X()), int(eventButton.Y()))
		if path.GetDepth() == 1 {
			ew.handleBaseRightClick(path, column)
		} else if path.GetDepth() == 2 {
			ew.handleChildRightClick(path, column)
		}
	}
}

func (ew *EnvironmentWindow) handleBaseRightClick(path *gtk.TreePath, column *gtk.TreeViewColumn) {
	env := ew.envs[path.GetIndices()[0]]

	rect := ew.envTreeView.GetCellArea(path, column)
	rect.SetY(rect.GetY() + 35)
	ew.actionPopover, _ = gtk.PopoverNew(ew.envTreeView)
	ew.actionPopover.SetPointingTo(*rect)
	ew.actionPopover.Add(NewBaseEnvironmentsActionBox(env, ew.doDeleteEnvironmentCallback, ew.doEditEnvironmentCallback, ew.doAddChildCallback))
	ew.actionPopover.ShowAll()
}

func (ew *EnvironmentWindow) handleChildRightClick(path *gtk.TreePath, column *gtk.TreeViewColumn) {
	env := ew.envs[path.GetIndices()[0]]

	rect := ew.envTreeView.GetCellArea(path, column)
	rect.SetY(rect.GetY() + 35)
	ew.actionPopover, _ = gtk.PopoverNew(ew.envTreeView)
	ew.actionPopover.SetPointingTo(*rect)
	ew.actionPopover.Add(NewChildEnvironmentsActionBox(env, ew.doDeleteEnvironmentCallback, ew.doEditEnvironmentCallback))
	ew.actionPopover.ShowAll()
}

func (ew *EnvironmentWindow) doDeleteEnvironmentCallback(env models.Environment) {
	newEnvs, err := ew.envManagementCallback.DoDeleteEnvironment(env)
	if err != nil {
		return
	}
	ew.setEnvironments(newEnvs)
	ew.actionPopover.Hide()
	ew.actionPopover.Destroy()
}

func (ew *EnvironmentWindow) doEditEnvironmentCallback(env models.Environment, name string) {
	ew.selectedEnvironment.Name = name
	newEnvs, err := ew.envManagementCallback.DoUpdateEnvironment(ew.selectedEnvironment)
	if err != nil {
		return
	}
	ew.setEnvironments(newEnvs)
	ew.actionPopover.Hide()
	ew.actionPopover.Destroy()
}

func (ew *EnvironmentWindow) doAddChildCallback(selectedEnv models.Environment, childName string) {
	var kvp []models.KVPair
	env := models.Environment{ID: 0, Name: childName, Pairs: kvp, ParentID: selectedEnv.ID}
	newEnvs, err := ew.envManagementCallback.DoAddEnvironment(env)
	if err != nil {
		return
	}
	ew.setEnvironments(newEnvs)
	ew.actionPopover.Hide()
	ew.actionPopover.Destroy()
}

func (ew *EnvironmentWindow) setEnvironments(envs []models.Environment) {
	ew.envs = envs
	ew.envTreeStore.Clear()
	if ew.envs != nil && len(ew.envs) > 0 {
		ew.envTreeStore.AddEnvironments(ew.envs)
		ew.envTreeView.ActivateFirst()
	}
}

func (ew *EnvironmentWindow) kvListUpdated() {
	ew.selectedEnvironment.Pairs = ew.kvList.GetPairs()
	ew.envManagementCallback.DoUpdateEnvironment(ew.selectedEnvironment)
	// Newly added kv pairs aren't shown until reloading the environment window.
	// This appears to be due to how go handles pointers to the items.  So I need to update the environment in the list
OuterLoop:
	for pID, parent := range ew.envs {

		if parent.ID == ew.selectedEnvironment.ID {
			ew.envs[pID] = ew.selectedEnvironment
			break
		}

		for cID, child := range parent.Children {
			if child.ID == ew.selectedEnvironment.ID {
				ew.envs[pID].Children[cID] = ew.selectedEnvironment
				break OuterLoop
			}
		}
	}
}
