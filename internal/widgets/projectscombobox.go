package widgets

import (
	"log"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type ProjectsComboBox struct {
	*gtk.ComboBox
	listStore *ProjectsListStore
}

func newProjectsComboBox(pls *ProjectsListStore) *ProjectsComboBox {
	comboBox, err := gtk.ComboBoxNew()
	if err != nil {
		panic(err)
	}
	pcb := &ProjectsComboBox{ComboBox: comboBox, listStore: pls}

	cell, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	pcb.PackStart(cell, true)
	pcb.AddAttribute(cell, "text", 0)

	pcb.SetModel(pcb.listStore)
	pcb.SetActive(0)

	return pcb
}

func (pcb *ProjectsComboBox) GetSelectedName() (string, error) {
	iter, err := pcb.GetActiveIter()
	if err != nil {
		return "", err
	}

	iModel, err := pcb.GetModel()
	if err != nil {
		return "", err
	}
	rawVal, err := iModel.ToTreeModel().GetValue(iter, 0)
	if err != nil {
		return "", err
	}

	return rawVal.GetString()
}

func (pcb *ProjectsComboBox) UpdateActiveName(projectName string) {
	iter, err := pcb.GetActiveIter()
	if err != nil {
		log.Println("error getting project name", err)
		return
	}
	pcb.listStore.UpdateProjectName(projectName, iter)
}

type ProjectsListStore struct {
	*gtk.ListStore
}

func newProjectsListStore() *ProjectsListStore {
	listStore, err := gtk.ListStoreNew(glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}
	pls := &ProjectsListStore{ListStore: listStore}

	return pls
}

func (pls *ProjectsListStore) AddProject(project models.Project) {
	iter := pls.Append()
	pls.SetValue(iter, 0, project.Name)
}

func (pls *ProjectsListStore) UpdateProjectName(projectName string, iter *gtk.TreeIter) {
	pls.SetValue(iter, 0, projectName)
}
