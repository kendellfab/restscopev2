package widgets

import (
	"fmt"
	"time"

	"gitlab.com/kendellfab/restscope/internal/style"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type HistoryListBoxItem struct {
	*gtk.ListBoxRow
	guiResponse models.GuiResponse
}

func newHistoryListBoxItem(guiResponse models.GuiResponse, deleteResponse func(response models.GuiResponse, item *HistoryListBoxItem), viewResponse func(response models.GuiResponse)) *HistoryListBoxItem {
	lbr, err := gtk.ListBoxRowNew()
	if err != nil {
		panic(err)
	}
	hlbi := &HistoryListBoxItem{ListBoxRow: lbr, guiResponse: guiResponse}

	hlbi.SetMarginTop(5)
	hlbi.SetMarginEnd(2)
	hlbi.SetMarginBottom(5)

	box, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		panic(err)
	}
	lblCode := NewColorLabel("--", style.COLOR_YELLOW)

	box.PackStart(lblCode, false, false, 0)
	lblCode.SetText(fmt.Sprintf("%d", guiResponse.Code))

	if guiResponse.Code >= 200 && guiResponse.Code < 300 {
		lblCode.SetColor(style.COLOR_GREEN)
	} else if guiResponse.Code >= 300 && guiResponse.Code < 400 {
		lblCode.SetColor(style.COLOR_BLUE)
	} else if guiResponse.Code >= 400 && guiResponse.Code < 500 {
		lblCode.SetColor(style.COLOR_ORANGE)
	} else {
		lblCode.SetColor(style.COLOR_RED)
	}

	duration := NewColorLabel(fmt.Sprintf("%d ms", guiResponse.RecvTime), style.COLOR_GRAY)
	box.PackStart(duration, false, false, 0)

	when := NewColorLabel(guiResponse.At.Format(time.RFC1123), style.COLOR_GRAY)
	box.PackStart(when, false, false, 0)

	btnDelete, err := gtk.ButtonNewFromIconName("edit-delete-symbolic", gtk.ICON_SIZE_MENU)
	if err != nil {
		panic(err)
	}
	btnDelete.SetMarginEnd(4)
	box.PackEnd(btnDelete, false, false, 0)

	btnView, _ := gtk.ButtonNewFromIconName("system-search", gtk.ICON_SIZE_MENU)
	btnView.SetMarginEnd(4)
	box.PackEnd(btnView, false, false, 0)

	btnDelete.Connect("clicked", func() {
		deleteResponse(guiResponse, hlbi)
	})

	// FIXME: I'd rather just have an on click in the historybox, but I don't know how to get the response
	// data out of the list box item
	btnView.Connect("clicked", func() {
		viewResponse(guiResponse)
	})

	hlbi.Add(box)

	return hlbi
}

func (hlbi *HistoryListBoxItem) GetResponse() models.GuiResponse {
	return hlbi.guiResponse
}
