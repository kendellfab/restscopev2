package widgets

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

var BodyTypes = []string{"None", "Multipart", "Form URL Encoded", "JSON", "XML", "YAML", "Other", "Binary File"}

type BodyTypeComboBox struct {
	*gtk.ComboBox
	btls *BodyTypeListStore
}

func newBodyTypeComboBox() *BodyTypeComboBox {
	comboBox, err := gtk.ComboBoxNew()
	if err != nil {
		panic(err)
	}
	btc := &BodyTypeComboBox{ComboBox: comboBox}
	btc.btls = newBodyTypeListStore()
	btc.SetModel(btc.btls)

	cell, err := gtk.CellRendererTextNew()
	if err != nil {
		panic(err)
	}
	btc.PackStart(cell, true)
	btc.AddAttribute(cell, "text", 0)
	btc.SetActive(0)

	return btc
}

func (btcb *BodyTypeComboBox) GetBodyType() string {
	active := btcb.GetActive()
	return BodyTypes[active]
}

type BodyTypeListStore struct {
	*gtk.ListStore
}

func newBodyTypeListStore() *BodyTypeListStore {
	listStore, err := gtk.ListStoreNew(glib.TYPE_STRING)
	if err != nil {
		panic(err)
	}
	btls := &BodyTypeListStore{ListStore: listStore}

	for _, bodyType := range BodyTypes {
		iter := btls.Append()
		btls.SetValue(iter, 0, bodyType)
	}

	return btls
}
