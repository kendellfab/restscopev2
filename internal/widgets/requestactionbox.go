package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/models"
)

type RequestActionBox struct {
	*gtk.Grid
}

func NewRequestActionBox(request models.StorageRequest, doEditRequestName func(name string), requestDelete func(req models.StorageRequest)) *RequestActionBox {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}
	rab := &RequestActionBox{Grid: grid}
	rab.SetMarginTop(10)
	rab.SetMarginStart(10)
	rab.SetMarginEnd(10)
	rab.SetMarginBottom(10)
	rab.SetRowSpacing(10)
	rab.SetColumnSpacing(5)

	editlabel, err := gtk.LabelNew("New Request Name")
	if err != nil {
		panic(err)
	}
	editEntry, err := gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	editEntry.SetPlaceholderText(request.Name)

	doEditButton, err := gtk.ButtonNewWithLabel("Edit Request")
	if err != nil {
		panic(err)
	}
	doEditButton.Connect("clicked", func() {
		name, _ := editEntry.GetText()
		doEditRequestName(name)
		editEntry.SetText("")
	})

	rab.Attach(editlabel, 0, 0, 1, 1)
	rab.Attach(editEntry, 1, 0, 1, 1)
	rab.Attach(doEditButton, 0, 1, 2, 1)

	sep, err := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	if err != nil {
		panic(err)
	}
	rab.Attach(sep, 0, 2, 2, 1)

	delButton, err := gtk.ButtonNewWithLabel("Delete")
	if err != nil {
		panic(err)
	}
	delButton.Connect("clicked", func() {
		requestDelete(request)
	})

	rab.Attach(delButton, 0, 3, 2, 1)

	return rab
}
