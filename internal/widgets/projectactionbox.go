package widgets

import "github.com/gotk3/gotk3/gtk"

type ProjectActionBox struct {
	*gtk.Grid
	doEditProject func(projectName string)

	editLabel *gtk.Label
	editEntry *gtk.Entry
}

func newProjectActionBox(doEditProject func(projectName string)) *ProjectActionBox {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}
	pab := &ProjectActionBox{Grid: grid, doEditProject: doEditProject}
	pab.SetMarginStart(10)
	pab.SetMarginTop(10)
	pab.SetMarginEnd(10)
	pab.SetMarginBottom(10)

	pab.SetRowSpacing(10)
	pab.SetColumnSpacing(5)

	pab.editLabel, err = gtk.LabelNew("New Project Name")
	if err != nil {
		panic(err)
	}
	pab.editEntry, err = gtk.EntryNew()
	if err != nil {
		panic(err)
	}
	doEditButton, err := gtk.ButtonNewWithLabel("Edit Project")
	if err != nil {
		panic(err)
	}
	doEditButton.Connect("clicked", func() {
		txt, _ := pab.editEntry.GetText()
		pab.doEditProject(txt)
		pab.editEntry.SetText("")
	})

	pab.Attach(pab.editLabel, 0, 0, 1, 1)
	pab.Attach(pab.editEntry, 1, 0, 1, 1)
	pab.Attach(doEditButton, 0, 2, 2, 1)

	return pab
}

func (pab *ProjectActionBox) setProjectName(projectName string) {
	pab.editEntry.SetPlaceholderText(projectName)
}
