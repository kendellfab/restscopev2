package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal/callbacks"
	"gitlab.com/kendellfab/restscope/internal/data"
	"gitlab.com/kendellfab/restscope/internal/models"
)

const (
	username = "Username"
	password = "Password"
	token    = "Token"
	prefix   = "Bearer prefix"
)

type AuthBox struct {
	*gtk.Box
	guiRequestUpdateCallback  callbacks.GuiRequestUpdateCallback
	authTypeComboBox          *AuthTypeComboBox
	grid                      *gtk.Grid
	nameLabel                 *gtk.Label
	nameEntry                 *gtk.Entry
	prefixLabel               *gtk.Label
	passwordBearerPrefixEntry *gtk.Entry
	enabled                   *gtk.CheckButton
}

func NewAuthBox(guiRequestUpdateCallback callbacks.GuiRequestUpdateCallback) *AuthBox {
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 2)
	if err != nil {
		panic(err)
	}
	ab := &AuthBox{Box: box, guiRequestUpdateCallback: guiRequestUpdateCallback}
	ab.authTypeComboBox = NewAuthTypeComboBox()
	ab.authTypeComboBox.SetMarginTop(2)
	ab.authTypeComboBox.SetMarginStart(2)
	ab.authTypeComboBox.SetMarginEnd(2)
	ab.authTypeComboBox.SetMarginBottom(5)
	ab.authTypeComboBox.Connect("changed", ab.authTypeChanged)

	ab.Add(ab.authTypeComboBox)

	return ab
}

func (ab *AuthBox) authTypeChanged() {
	if ab.grid == nil {
		ab.initGrid()
	}
	at := ab.GetAuthType()
	switch at {
	case data.AuthNone:
		ab.grid.SetVisible(false)
		ab.enabled.SetVisible(false)
	case data.Basic:
		ab.grid.SetVisible(true)
		ab.nameLabel.SetText(username)
		ab.prefixLabel.SetText(password)
		ab.passwordBearerPrefixEntry.SetPlaceholderText("")
		ab.enabled.SetActive(true)
	case data.Bearer:
		ab.grid.SetVisible(true)
		ab.nameLabel.SetText(token)
		ab.prefixLabel.SetText(prefix)
		ab.passwordBearerPrefixEntry.SetPlaceholderText("Bearer if left empty")
		ab.enabled.SetActive(true)
	}
}

func (ab *AuthBox) initGrid() {
	grid, err := gtk.GridNew()
	if err != nil {
		panic(err)
	}
	ab.grid = grid
	grid.SetMarginTop(5)
	grid.SetMarginStart(5)
	grid.SetMarginEnd(5)
	grid.SetMarginBottom(5)
	grid.SetRowSpacing(10)
	grid.SetColumnSpacing(5)

	ab.nameLabel, _ = gtk.LabelNew(username)
	ab.nameEntry, _ = gtk.EntryNew()
	ab.nameEntry.Connect("focus-out-event", ab.handleFocusOut)
	ab.nameEntry.SetHExpand(true)

	ab.prefixLabel, _ = gtk.LabelNew(password)
	ab.passwordBearerPrefixEntry, _ = gtk.EntryNew()
	ab.passwordBearerPrefixEntry.Connect("focus-out-event", ab.handleFocusOut)
	ab.passwordBearerPrefixEntry.SetHExpand(true)
	grid.Attach(ab.nameLabel, 0, 0, 1, 1)
	grid.Attach(ab.nameEntry, 1, 0, 1, 1)
	grid.Attach(ab.prefixLabel, 0, 1, 1, 1)
	grid.Attach(ab.passwordBearerPrefixEntry, 1, 1, 1, 1)

	ab.enabled, _ = gtk.CheckButtonNewWithLabel("Enabled")
	ab.enabled.Connect("toggled", ab.handleFocusOut)

	grid.Attach(ab.enabled, 0, 2, 2, 1)

	ab.Add(grid)
	ab.grid.ShowAll()
}

func (ab *AuthBox) handleFocusOut() {
	if ab.guiRequestUpdateCallback != nil {
		ab.guiRequestUpdateCallback()
	}
}

func (ab *AuthBox) GetAuthType() data.AuthType {
	active := ab.authTypeComboBox.GetActive()
	return data.AuthType(active)
}

func (ab *AuthBox) Reset() {
	if ab.nameEntry != nil {
		ab.nameEntry.SetText("")
	}
	if ab.passwordBearerPrefixEntry != nil {
		ab.passwordBearerPrefixEntry.SetText("")
	}
	ab.authTypeComboBox.SetActive(0)
}

func (ab *AuthBox) GetValues() *models.KVPair {
	key := ""
	if ab.nameEntry != nil {
		key, _ = ab.nameEntry.GetText()
	}
	val := ""
	if ab.passwordBearerPrefixEntry != nil {
		val, _ = ab.passwordBearerPrefixEntry.GetText()
	}
	active := false
	if ab.enabled != nil {
		active = ab.enabled.GetActive()
	}
	return &models.KVPair{Key: key, Value: val, Active: active}
}

func (ab *AuthBox) UpdateAuth(at data.AuthType, auth *models.KVPair) {
	if at == data.Basic {
		ab.authTypeComboBox.SetActive(1)
		ab.nameEntry.SetText(auth.Key)
		ab.passwordBearerPrefixEntry.SetText(auth.Value)
		ab.enabled.SetActive(auth.Active)
	} else if at == data.Bearer {
		ab.authTypeComboBox.SetActive(2)
		ab.nameEntry.SetText(auth.Key)
		ab.passwordBearerPrefixEntry.SetText(auth.Value)
		ab.enabled.SetActive(auth.Active)
	}
}
