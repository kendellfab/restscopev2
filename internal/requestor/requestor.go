package requestor

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptrace"
	stdurl "net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/kendellfab/restscope/internal/data"

	"github.com/aymerick/raymond"
	"gitlab.com/kendellfab/restscope/internal/models"
)

const (
	contentType   = "Content-Type"
	contentLength = "Content-Length"
)

func MakeRequest(request models.GuiRequest, activeEnv map[string]string) models.GuiResponse {

	/*  Begin interpolating URL */
	url, urlErr := raymond.Render(request.URL, activeEnv)
	if urlErr != nil {
		return models.GuiResponse{ErrorMessage: urlErr.Error()}
	}
	/* End URL */
	log.Println(fmt.Sprintf("---- Starting Request : %s ----", url))

	/* Query Params */
	interpolatedQuery := make(map[string]string)
	for _, kvp := range request.Query {
		if kvp.Active {
			interpolatedQuery[kvp.Key] = interpolateParameter(kvp.Value, activeEnv)
		}
	}
	/* End Query Params */

	/* Headers */
	headers := make(map[string]string)
	headers["user-agent"] = "RestScopeV2"
	for _, kvp := range request.Headers {
		if kvp.Active {
			headers[kvp.Key] = interpolateParameter(kvp.Value, activeEnv)
		}
	}
	/* End Headers */

	// Figure out body if required
	var bodyInput io.Reader
	if request.Method == "POST" || request.Method == "PUT" {
		switch request.BodyType {
		case data.Multipart:
			var bodyBuff bytes.Buffer
			writer := multipart.NewWriter(&bodyBuff)
			headers[contentType] = writer.FormDataContentType()
			for _, pair := range request.BodyPairs {
				if pair.Active {
					writer.WriteField(pair.Key, interpolateParameter(pair.Value, activeEnv))
				}
			}
			writer.Close()
			bodyInput = bytes.NewBuffer(bodyBuff.Bytes())

		case data.Form:
			headers[contentType] = "application/x-www-form-urlencoded"
			form := stdurl.Values{}
			for _, pair := range request.BodyPairs {
				if pair.Active {
					form.Set(pair.Key, interpolateParameter(pair.Value, activeEnv))
				}
			}
			bodyInput = strings.NewReader(form.Encode())
			headers[contentLength] = strconv.Itoa(len(form.Encode()))
		case data.JSON:
			headers[contentType] = "application/json"
			headers[contentLength] = strconv.Itoa(len(request.BodyRaw))
			bodyInput = bytes.NewBufferString(request.BodyRaw)
		case data.XML:
			headers[contentType] = "application/xml"
			headers[contentLength] = strconv.Itoa(len(request.BodyRaw))
			bodyInput = bytes.NewBufferString(request.BodyRaw)
		case data.YAML:
			headers[contentType] = "application/yaml"
			headers[contentLength] = strconv.Itoa(len(request.BodyRaw))
			bodyInput = bytes.NewBufferString(request.BodyRaw)
		case data.OTHER:
			headers[contentType] = "application/octet-stream"
			headers[contentLength] = strconv.Itoa(len(request.BodyRaw))
			bodyInput = bytes.NewBufferString(request.BodyRaw)
		case data.FILE:
			var fileBuff bytes.Buffer
			writer := multipart.NewWriter(&fileBuff)
			headers[contentType] = writer.FormDataContentType()
			field := "file"
			if request.FileKey != "" {
				field = request.FileKey
			}
			if fileWriter, fwErr := writer.CreateFormFile(field, request.FilePath); fwErr == nil {
				if inputFile, inputErr := os.Open(request.FilePath); inputErr == nil {
					io.Copy(fileWriter, inputFile)
					writer.Close()
					bodyInput = bytes.NewBuffer(fileBuff.Bytes())
				} else {
					return models.GuiResponse{ErrorMessage: inputErr.Error()}
				}
			} else {
				return models.GuiResponse{ErrorMessage: fwErr.Error()}
			}
		}
	}

	req, err := http.NewRequest(request.Method, url, bodyInput)
	if err != nil {
		return models.GuiResponse{ErrorMessage: err.Error()}
	}

	/* Auth Type */
	if request.AuthType == data.Basic {
		if request.Auth.Active {
			req.SetBasicAuth(request.Auth.Key, interpolateParameter(request.Auth.Value, activeEnv))
		}
	} else if request.AuthType == data.Bearer {
		if request.Auth.Active {
			bearerKey := "Bearer"
			if request.Auth.Value != "" {
				bearerKey = fmt.Sprintf("%s ", request.Auth.Value)
			}
			headers["Authorization"] = fmt.Sprintf("%s %s", bearerKey, interpolateParameter(request.Auth.Key, activeEnv))
		}

	}
	/* End Auth */

	// Setting headers on request
	for k, val := range headers {
		req.Header.Add(k, val)
	}

	// Adding query params to request
	q := stdurl.Values{}
	for k, val := range interpolatedQuery {
		q.Add(k, val)
	}
	req.URL.RawQuery = q.Encode()

	var start time.Time
	var dnsStart time.Time
	var dnsDone time.Time
	var connectStart time.Time
	var connectDone time.Time
	var firstByte time.Time
	trace := &httptrace.ClientTrace{
		DNSStart: func(info httptrace.DNSStartInfo) {
			dnsStart = time.Now()
			log.Println("DNS Start:", dnsStart, info)
		},
		DNSDone: func(info httptrace.DNSDoneInfo) {
			dnsDone = time.Now()
			log.Println("DNS Done:", dnsDone, info)
		},
		ConnectStart: func(network, addr string) {
			connectStart = time.Now()
			log.Println("Connect Start:", connectStart, network, addr)
		},
		ConnectDone: func(network, addr string, err error) {
			connectDone = time.Now()
			log.Println("Connect Done:", connectDone, network, addr, err)
		},
		GotFirstResponseByte: func() {
			firstByte = time.Now()
			log.Println("First Byte:", firstByte)
		},
	}
	req = req.WithContext(httptrace.WithClientTrace(req.Context(), trace))

	tr := &http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}

	client := &http.Client{
		Transport: tr,
	}

	start = time.Now()
	resp, err := client.Do(req)
	if err != nil {
		return models.GuiResponse{ErrorMessage: err.Error()}
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return models.GuiResponse{ErrorMessage: err.Error()}
	}
	end := time.Now()
	log.Println("Complete:", end)

	log.Println(fmt.Sprintf("---- Request Complete Connect: %d  Recv: %d ----", connectDone.Sub(start).Milliseconds(), end.Sub(start).Milliseconds()))

	respHeaders := make(map[string]string)
	for k, v := range resp.Header {
		respHeaders[k] = v[0]
	}

	return models.GuiResponse{Body: strings.ToValidUTF8(string(body), ""), URL: req.URL.String(), Headers: respHeaders, Code: resp.StatusCode,
		DnsStart: dnsStart.Sub(start).Milliseconds(), DnsDone: dnsDone.Sub(start).Milliseconds(), ConnectTime: connectDone.Sub(start).Milliseconds(), FirstByte: firstByte.Sub(start).Milliseconds(), RecvTime: end.Sub(start).Milliseconds(), Size: int64(len(body))}

}

func interpolateParameter(parameter string, activeEnv map[string]string) string {
	if strings.HasPrefix(parameter, "{{") && strings.HasSuffix(parameter, "}}") {
		parameter = strings.TrimPrefix(parameter, "{{")
		parameter = strings.TrimSuffix(parameter, "}}")
		if val, ok := activeEnv[parameter]; ok {
			return val
		} else {
			return ""
		}
	}
	return parameter
}
