package models

import "time"

type GuiResponse struct {
	Code         int               `json:"code"`
	DnsStart     int64             `json:"dnsStart"`
	DnsDone      int64             `json:"dnsDone"`
	ConnectTime  int64             `json:"connectTime"`
	FirstByte    int64             `json:"firstByte"`
	RecvTime     int64             `json:"recvTime"`
	Headers      map[string]string `json:"headers"`
	Body         string            `json:"body"`
	URL          string            `json:"url"`
	ErrorMessage string            `json:"errorMessage"`
	Size         int64             `json:"size"`
	ResponseID   int64             `json:"-"`
	At           time.Time         `json:"-"`
}
