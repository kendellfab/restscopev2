package models

type Project struct {
	ID   int64
	Name string
}

func NewProject(name string) Project {
	return Project{Name: name}
}
