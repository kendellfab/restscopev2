package models

type Folder struct {
	ID        int64
	Name      string
	ProjectID int64

	Requests []StorageRequest
}
