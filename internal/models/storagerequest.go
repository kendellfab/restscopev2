package models

type StorageRequest struct {
	ID       int64
	Name     string
	FolderID int64

	GuiRequest *GuiRequest
}
