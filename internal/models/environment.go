package models

import "fmt"

type Environment struct {
	ID       int64
	Name     string
	Pairs    []KVPair
	ParentID int64

	Parent   *Environment
	Children []Environment
}

func NewEnvironment(id int64, name string, pairs []KVPair) Environment {
	return Environment{ID: id, Name: name, Pairs: pairs, ParentID: -1}
}

func NewChildEnvironment(id int64, name string, pairs []KVPair, parentID int64) Environment {
	env := NewEnvironment(id, name, pairs)
	env.ParentID = parentID
	return env
}

func (e Environment) GetActiveEnv() map[string]string {
	res := make(map[string]string)
	if e.Parent != nil {
		res = e.Parent.GetActiveEnv()
	}
	for _, pair := range e.Pairs {
		if pair.Active {
			res[pair.Key] = pair.Value
		}
	}
	return res
}

func (e Environment) String() string {
	return fmt.Sprintf("ID: %d -- Name: %s -- ParentID: %d -- Pairs: %v", e.ID, e.Name, e.ParentID, e.Pairs)
}
