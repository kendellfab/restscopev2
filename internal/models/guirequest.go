package models

import "gitlab.com/kendellfab/restscope/internal/data"

type GuiRequest struct {
	URL       string        `json:"url"`
	Method    string        `json:"method"`
	Query     []KVPair      `json:"query"`
	Headers   []KVPair      `json:"headers"`
	BodyType  data.BodyType `json:"bodyType"`
	BodyPairs []KVPair      `json:"bodyPairs"`
	BodyRaw   string        `json:"bodyRaw"`
	AuthType  data.AuthType `json:"authType"`
	Auth      *KVPair       `json:"auth"`
	Notes     string        `json:"notes"`
	FilePath  string        `json:"filePath"`
	FileKey   string        `json:"fileKey"`
}

func NewGuiRequest(url, method string) *GuiRequest {
	return &GuiRequest{URL: url, Method: method}
}

func (gr *GuiRequest) SetQueryParams(query []KVPair) *GuiRequest {
	gr.Query = query
	return gr
}

func (gr *GuiRequest) SetHeaders(headers []KVPair) *GuiRequest {
	gr.Headers = headers
	return gr
}
