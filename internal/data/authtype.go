package data

import (
	"encoding/json"
	"strings"
)

type AuthType int

const (
	AuthNone AuthType = iota
	Basic
	Bearer
)

func (at *AuthType) UnmarshalJSON(b []byte) error {
	input := strings.Trim(string(b), `"`)

	var observed AuthType
	switch input {
	case "None":
		observed = AuthNone
	case "Basic":
		observed = Basic
	case "Bearer":
		observed = Bearer
	}
	*at = observed
	return nil
}

func (at AuthType) MarshalJSON() ([]byte, error) {
	var val string
	switch at {
	case AuthNone:
		val = "None"
	case Basic:
		val = "Basic"
	case Bearer:
		val = "Bearer"
	}
	return json.Marshal(val)
}
