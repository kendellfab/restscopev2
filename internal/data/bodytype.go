package data

import (
	"encoding/json"
	"strings"
)

type BodyType int

const (
	bodyNone  = "None"
	multipart = "Multipart"
	form      = "Form"
	tJson     = "JSON"
	xml       = "XML"
	yaml      = "YAML"
	other     = "OTHER"
	file      = "File"
)

const (
	BodyNone BodyType = iota
	Multipart
	Form
	JSON
	XML
	YAML
	OTHER
	FILE
)

func (bt BodyType) MarshalJSON() ([]byte, error) {
	var val string
	switch bt {
	case BodyNone:
		val = bodyNone
	case Multipart:
		val = multipart
	case Form:
		val = form
	case JSON:
		val = tJson
	case XML:
		val = xml
	case YAML:
		val = yaml
	case OTHER:
		val = other
	case FILE:
		val = file
	}
	return json.Marshal(val)
}

func (bt *BodyType) UnmarshalJSON(b []byte) error {
	input := strings.Trim(string(b), `"`)

	var observed BodyType
	switch input {
	case bodyNone:
		observed = BodyNone
	case multipart:
		observed = Multipart
	case form:
		observed = Form
	case tJson:
		observed = JSON
	case xml:
		observed = XML
	case yaml:
		observed = YAML
	case other:
		observed = OTHER
	case file:
		observed = FILE
	}
	*bt = observed
	return nil
}
