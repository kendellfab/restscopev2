package style

var Style = `
#red {
	background-color: rgb(194, 39, 12);
}

#yellow {
	background-color: rgb(252, 186, 3);
	color: #323232;
}

#green {
	background-color: rgb(12, 194, 51);
	color: #323232;
}

#orange {
	background-color: rgb(240, 156, 22);
}

#gray {
	background-color: rgb(209, 209, 209);
	color: #323232;
}

#blue {
	background-color: rgb(79, 165, 227);
}
`

const (
	COLOR_WHITE    = "white"
	COLOR_YELLOW   = "yellow"
	COLOR_RED      = "red"
	COLOR_GREEN    = "green"
	COLOR_GRAY     = "gray"
	COLOR_BLUE     = "blue"
	COLOR_ORANGE   = "orange"
	COLOR_DARKGRAY = "darkGray"
)
