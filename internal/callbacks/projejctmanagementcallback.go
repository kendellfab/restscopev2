package callbacks

import "gitlab.com/kendellfab/restscope/internal/models"

type ProjectManagementCallback interface {
	DoAddProject(project models.Project) int64
	DoAddFolder(folder models.Folder) int64
	SetProjectSelected(project models.Project)
	DoAddRequest(request models.StorageRequest, projectID int64) int64
	SetRequestSelected(request models.StorageRequest)
	DoFolderDelete(folder models.Folder, projectID int64)
	DoRequestDelete(request models.StorageRequest, projectID int64)
	DoUpdateFolder(folder models.Folder) bool
	DoUpdateProject(project models.Project) bool
	DoUpdateRequest(request models.StorageRequest) bool
}
