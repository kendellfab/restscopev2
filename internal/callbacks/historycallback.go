package callbacks

import "gitlab.com/kendellfab/restscope/internal/models"

type HistoryCallback interface {
	ListResponses() ([]models.GuiResponse, error)
	ResponsesForRequestReverse(limit int) ([]models.GuiResponse, error)
	DeleteResponse(response models.GuiResponse) error
	ClearAllResponsesForRequest() error
	ResponseSelected(response models.GuiResponse)
}
