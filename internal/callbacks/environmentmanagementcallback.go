package callbacks

import "gitlab.com/kendellfab/restscope/internal/models"

type EnvironmentManagementCallback interface {
	DoAddEnvironment(env models.Environment) ([]models.Environment, error)
	DoUpdateEnvironment(env models.Environment) ([]models.Environment, error)
	DoDeleteEnvironment(env models.Environment) ([]models.Environment, error)
}
