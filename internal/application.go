package internal

import (
	"log"
	"time"

	"gitlab.com/kendellfab/restscope/internal/style"

	"github.com/gotk3/gotk3/gdk"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"github.com/hfmrow/gotk3_gtksource/source"
	"gitlab.com/kendellfab/restscope/internal/models"
	"gitlab.com/kendellfab/restscope/internal/requestor"
	"gitlab.com/kendellfab/restscope/internal/settings"
	"gitlab.com/kendellfab/restscope/internal/storage"
	"gitlab.com/kendellfab/restscope/internal/widgets"
)

const (
	ACTION_ACTIVATE = "activate"
	ACTION_CLICKED  = "clicked"
	SPACING         = 2
)

type Application struct {
	mainApplication *gtk.Application
	mainWindow      *gtk.ApplicationWindow
	settingsManager settings.Manager
	storageManager  *storage.Manager

	headerBar *gtk.HeaderBar

	envButton   *gtk.Button
	envCombobox *widgets.EnvironmentsComboBox

	methodCombo   *widgets.MethodComboBox
	urlEntry      *gtk.Entry
	entryNotebook *gtk.Notebook

	queryValueList  *widgets.KeyValueList
	headerValueList *widgets.KeyValueList
	bodyBox         *widgets.BodyBox
	authBox         *widgets.AuthBox
	notes           *source.SourceView

	projectBox *widgets.ProjectBox

	resultsBox *widgets.ResultsBox

	isUpdating             bool
	selectedStorageRequest *models.StorageRequest

	environmentWindow  *widgets.EnvironmentWindow
	styleSchemeManager *source.SourceStyleSchemeManager

	resizePending bool
}

func NewApplication(title string, mainApplication *gtk.Application, settingsManager settings.Manager) *Application {
	win, err := gtk.ApplicationWindowNew(mainApplication)
	if err != nil {
		log.Fatal("error creating main window", err)
	}

	if screen, err := gdk.ScreenGetDefault(); err == nil {
		if cssProvider, err := gtk.CssProviderNew(); err == nil {
			if cssErr := cssProvider.LoadFromData(style.Style); cssErr == nil {
				gtk.AddProviderForScreen(screen, cssProvider, gtk.STYLE_PROVIDER_PRIORITY_USER)
			} else {
				log.Println("Css Error", cssErr)
			}

		}
	}

	win.Connect("destroy", func() {
		gtk.MainQuit()
	})

	win.SetIcon(mustIcon())

	app := &Application{mainApplication: mainApplication, mainWindow: win, settingsManager: settingsManager}
	app.styleSchemeManager, err = source.SourceStyleSchemeManagerNew()
	app.setupHeaderBar(title)
	app.setupMainLayout()

	if err != nil {
		log.Println("error loading style manager", err)
	}

	lastDB, err := settingsManager.GetLastDB()
	if err == nil && lastDB != "" {
		app.openDBPath(lastDB)
	}

	win.Connect("configure-event", func() {
		if app.resizePending {
			return
		}

		app.resizePending = true

		go func() {
			time.Sleep(300 * time.Millisecond)
			if app.selectedStorageRequest != nil {
				log.Println("Window -> Width:", win.GetAllocatedWidth(), "Height:", win.GetAllocatedHeight())
				glib.IdleAdd(func() {
					app.resultsBox.NotifyResize(app.selectedStorageRequest)
				})
			}
			app.resizePending = false
		}()
	})

	return app
}

func (a *Application) Display(width, height int) {
	a.mainWindow.SetDefaultSize(width, height)
	a.mainWindow.ShowAll()
}

func (a *Application) updateSchemes() {
	scheme := a.getScheme()
	a.resultsBox.UpdateScheme(scheme)
	if noteBuff, err := a.notes.GetBuffer(); err == nil {
		noteBuff.SetStyleSheme(scheme)
	}
	a.bodyBox.SetScheme(scheme)
}

func (a *Application) getScheme() *source.SourceStyleScheme {
	allSchemes := a.styleSchemeManager.GetShemeIds()
	if schemeID, err := a.settingsManager.GetSchemeID(); err == nil && schemeID != "" {
		if s, e := a.styleSchemeManager.GetScheme(schemeID); e == nil {
			return s
		}
	}
	s, _ := a.styleSchemeManager.GetScheme(allSchemes[0])
	return s
}

func (a *Application) setupHeaderBar(title string) {

	resetAction := glib.SimpleActionNew("reset", nil)
	resetAction.Connect(ACTION_ACTIVATE, func() {
		log.Println("Reset Activated")
	})
	a.mainWindow.AddAction(resetAction)

	themeAction := glib.SimpleActionNew("theme", nil)
	themeAction.Connect(ACTION_ACTIVATE, func() {
		log.Println("Theme activated")

		schemeChooser, err := source.SourceStyleSchemeChooserWidgetNew()

		if err != nil {
			log.Println("error creating scheme chooser", err)
			return
		}

		schemeChooser.Connect("notify::style-scheme", func() {
			log.Println("Scheme selected")
			if scheme, err := schemeChooser.GetStyleScheme(); err == nil {
				a.settingsManager.SetSchemeID(scheme.GetId())
				a.updateSchemes()
			}
		})

		schemeDialog, err := gtk.DialogNew()
		if err != nil {
			log.Println("error creating dialog", err)
			return
		}
		schemeDialog.SetTitle("Scheme Chooser")
		box, _ := schemeDialog.GetContentArea()
		box.Add(schemeChooser)
		schemeDialog.ShowAll()
	})
	a.mainWindow.AddAction(themeAction)

	aboutAction := glib.SimpleActionNew("about", nil)
	aboutAction.Connect(ACTION_ACTIVATE, func() {
		log.Println("About dialog")
	})
	a.mainWindow.AddAction(aboutAction)

	hb, err := gtk.HeaderBarNew()
	if err != nil {
		log.Fatal(err)
	}

	hb.SetShowCloseButton(true)
	hb.SetTitle(title)
	hb.SetSubtitle("Displaying a subtitle")

	/* View More Button */
	viewMore, err := gtk.MenuButtonNew()
	if err != nil {
		log.Fatal(err)
	}
	viewMoreImg, err := gtk.ImageNewFromIconName("view-more", gtk.ICON_SIZE_LARGE_TOOLBAR)
	if err != nil {
		log.Fatal(err)
	}
	viewMore.Add(viewMoreImg)
	viewMore.SetVAlign(gtk.ALIGN_CENTER)
	hb.PackEnd(viewMore)

	viewMoreMenu := glib.MenuNew()
	viewMoreMenu.Insert(0, "Reset", "win.reset")
	viewMoreMenu.Insert(1, "Theme", "win.theme")
	viewMoreMenu.Insert(2, "About", "win.about")

	viewMorePopover, err := gtk.PopoverNewFromModel(viewMore, &viewMoreMenu.MenuModel)
	panicIf(err)
	viewMore.SetPopover(viewMorePopover)

	/* Files Button */
	filesButton, err := gtk.MenuButtonNew()
	if err != nil {
		log.Println(err)
	}
	filesButton.SetVAlign(gtk.ALIGN_CENTER)
	filesImg, err := gtk.ImageNewFromIconName("pan-down", gtk.ICON_SIZE_LARGE_TOOLBAR)
	panicIf(err)
	filesButton.Add(filesImg)
	hb.PackStart(filesButton)

	newAction := glib.SimpleActionNew("new", nil)
	newAction.Connect(ACTION_ACTIVATE, func() {
		fd, err := gtk.FileChooserNativeDialogNew("New Database", a.mainWindow, gtk.FILE_CHOOSER_ACTION_SAVE, "Save", "Cancel")
		if err != nil {
			log.Println("error creating save dialog", err)
			return
		}

		response := fd.Run()
		if response == int(gtk.RESPONSE_ACCEPT) {
			fileName := fd.GetFilename()
			fileName += ".restscope"

			if a.storageManager != nil {
				a.reset()
				a.storageManager.Close()
				a.storageManager = nil
			}

			a.settingsManager.SetLastDB(fileName)
			a.storageManager, err = storage.NewManager(fileName)
			a.projectBox.SetProjects([]models.Project{})
			a.envCombobox.Reset()
			a.envButton.SetSensitive(true)
		}
		fd.Destroy()
	})
	a.mainWindow.AddAction(newAction)
	openAction := glib.SimpleActionNew("open", nil)
	openAction.Connect(ACTION_ACTIVATE, func() {
		fd, err := gtk.FileChooserNativeDialogNew("Open Database", a.mainWindow, gtk.FILE_CHOOSER_ACTION_OPEN, "Open", "Cancel")
		if err != nil {
			log.Println("error showing file chooser", err)
			return
		}

		fd.SetLocalOnly(true)
		filter, err := gtk.FileFilterNew()
		if err != nil {
			log.Println("error creating filter", err)
			return
		}
		filter.AddPattern("*.restscope")
		filter.SetName("Rest Scope")
		fd.AddFilter(filter)
		response := fd.Run()
		if response == int(gtk.RESPONSE_ACCEPT) {
			a.reset()
			a.openDBPath(fd.GetFilename())
		}
		fd.Destroy()
	})
	a.mainWindow.AddAction(openAction)
	filesMenu := glib.MenuNew()
	filesMenu.Insert(0, "Open", "win.open")
	filesMenu.Insert(1, "New", "win.new")

	filesPopover, err := gtk.PopoverNewFromModel(filesButton, &filesMenu.MenuModel)
	panicIf(err)
	filesButton.SetPopover(filesPopover)

	/* Environment Button */
	envButton, err := gtk.ButtonNewFromIconName("open-menu", gtk.ICON_SIZE_LARGE_TOOLBAR)
	panicIf(err)
	envButton.SetVAlign(gtk.ALIGN_CENTER)
	envButton.SetSensitive(false)
	hb.PackEnd(envButton)
	envButton.Connect(ACTION_CLICKED, a.envButtonClicked)
	a.envButton = envButton

	a.envCombobox = widgets.NewEnvironmentsCombobox()
	hb.PackEnd(a.envCombobox)

	a.headerBar = hb
	a.mainWindow.SetTitlebar(hb)
}

func (a *Application) setupMainLayout() {
	vBox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, SPACING)
	panicIf(err)

	vBox.SetMarginStart(10)
	vBox.SetMarginEnd(10)
	vBox.SetHExpand(true)

	entryBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, SPACING)
	panicIf(err)

	a.methodCombo = widgets.NewMethodComboBox()
	a.methodCombo.SetHExpand(true)
	a.methodCombo.Connect("changed", func() {
		log.Println(a.methodCombo.GetMethod())
	})

	a.urlEntry, err = gtk.EntryNew()
	panicIf(err)
	a.urlEntry.SetPlaceholderText("Enter URL...")
	a.urlEntry.SetMarginStart(5)
	a.urlEntry.SetMarginEnd(5)
	a.urlEntry.SetHExpand(true)
	a.urlEntry.Connect("focus-out-event", func() {
		log.Println("Focus out")
	})

	goButton, err := gtk.ButtonNewWithLabel("Go")
	panicIf(err)
	goButton.Connect("clicked", a.goButton)
	entryBox.Add(a.methodCombo)
	entryBox.Add(a.urlEntry)
	entryBox.Add(goButton)
	entryBox.SetHExpand(true)
	vBox.Add(entryBox)

	projSep, err := gtk.SeparatorNew(gtk.ORIENTATION_HORIZONTAL)
	panicIf(err)
	projSep.SetMarginTop(10)
	projSep.SetMarginBottom(10)
	vBox.Add(projSep)

	a.entryNotebook, err = gtk.NotebookNew()
	panicIf(err)
	a.entryNotebook.SetVExpand(true)
	a.entryNotebook.SetMarginTop(8)

	a.queryValueList = widgets.NewKeyValueList(a.guiUpdate, nil, nil)
	lblQuery, err := gtk.LabelNew("Query Params")
	panicIf(err)
	a.entryNotebook.AppendPage(a.queryValueList, lblQuery)

	a.headerValueList = widgets.NewKeyValueList(a.guiUpdate, nil, nil)
	lblHeaders, err := gtk.LabelNew("Headers")
	panicIf(err)
	a.entryNotebook.AppendPage(a.headerValueList, lblHeaders)

	a.bodyBox = widgets.NewBodyBox(a.guiUpdate, a.GetBodyFilePath)
	lblBodyBox, err := gtk.LabelNew("Body")
	panicIf(err)
	a.entryNotebook.AppendPage(a.bodyBox, lblBodyBox)

	a.authBox = widgets.NewAuthBox(a.guiUpdate)
	lblAuthBox, err := gtk.LabelNew("Authentication")
	panicIf(err)
	a.entryNotebook.AppendPage(a.authBox, lblAuthBox)

	a.notes, err = source.SourceViewNew()
	panicIf(err)
	a.notes.SetMarginTop(2)
	// TODO: Add focus lost handler
	a.notes.SetShowLineNumbers(true)
	a.notes.SetWrapMode(gtk.WRAP_WORD)
	a.notes.SetHighlightCurrentLine(true)
	scrollWindow, err := gtk.ScrolledWindowNew(nil, nil)
	panicIf(err)
	scrollWindow.Add(a.notes)
	lblNotes, err := gtk.LabelNew("Notes")
	panicIf(err)
	a.entryNotebook.AppendPage(scrollWindow, lblNotes)

	vBox.Add(a.entryNotebook)

	a.resultsBox = widgets.NewResultsBox(a)
	panicIf(err)

	insidePaned, err := gtk.PanedNew(gtk.ORIENTATION_HORIZONTAL)
	panicIf(err)
	insidePaned.Pack1(vBox, false, false)
	insidePaned.Pack2(a.resultsBox, true, true)
	insidePaned.SetMarginStart(10)

	a.projectBox = widgets.NewProjectBox(a.guiUpdate, a)
	panicIf(err)

	outsidePaned, err := gtk.PanedNew(gtk.ORIENTATION_HORIZONTAL)
	panicIf(err)
	outsidePaned.SetMarginTop(10)
	outsidePaned.SetMarginBottom(10)
	outsidePaned.SetMarginStart(10)
	outsidePaned.SetMarginEnd(10)
	outsidePaned.Pack1(a.projectBox, false, false)
	outsidePaned.Pack2(insidePaned, true, true)
	a.mainWindow.Add(outsidePaned)
	a.updateSchemes()
}

func (a *Application) envButtonClicked() {
	if a.environmentWindow != nil {
		return
	}

	envs, err := a.storageManager.ListHierarchy()
	if err != nil {
		a.showErrorDialog(err.Error())
		return
	}
	a.environmentWindow = widgets.NewEnvironmentWindow(envs, a)
	a.environmentWindow.ShowAll()
	a.environmentWindow.Present()
	a.environmentWindow.Connect("destroy", func() {
		a.environmentWindow = nil
	})
}

func (a *Application) DoAddEnvironment(env models.Environment) ([]models.Environment, error) {
	a.storageManager.AddEnvironment(env)
	envs, err := a.storageManager.ListHierarchy()
	if err != nil {
		a.showErrorDialog(err.Error())
		return envs, err
	}
	a.envCombobox.AddEnvironments(envs)
	return envs, err
}

func (a *Application) DoUpdateEnvironment(env models.Environment) ([]models.Environment, error) {
	a.storageManager.UpdateEnvironment(env)
	envs, err := a.storageManager.ListHierarchy()
	if err != nil {
		a.showErrorDialog(err.Error())
		return envs, err
	}
	a.envCombobox.AddEnvironments(envs)
	return envs, err
}

func (a *Application) DoDeleteEnvironment(env models.Environment) ([]models.Environment, error) {
	a.storageManager.DeleteEnvironment(env)
	envs, err := a.storageManager.ListHierarchy()
	if err != nil {
		a.showErrorDialog(err.Error())
		return envs, err
	}
	a.envCombobox.AddEnvironments(envs)
	return envs, err
}

func (a *Application) openDBPath(path string) {
	if a.storageManager != nil {
		a.storageManager.Close()
		a.storageManager = nil
	}
	var err error
	a.settingsManager.SetLastDB(path)
	a.storageManager, err = storage.NewManager(path)
	if err != nil {
		log.Println("Error opening database", err)
		a.showErrorDialog(err.Error())
		return
	}
	projects, projErr := a.storageManager.ListProjects()
	if projErr != nil {
		a.showErrorDialog(projErr.Error())
		a.settingsManager.SetLastDB("")
		return
	}
	a.projectBox.SetProjects(projects)
	envs, envErr := a.storageManager.ListHierarchy()
	if envErr != nil {
		a.showErrorDialog(envErr.Error())
		a.settingsManager.SetLastDB("")
		return
	}
	log.Println(envs)
	a.envCombobox.AddEnvironments(envs)
	a.envButton.SetSensitive(true)
}

func (a *Application) showErrorDialog(message string) {
	msgDialog := gtk.MessageDialogNew(a.mainWindow, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR, gtk.BUTTONS_CLOSE, message)
	msgDialog.Run()
	msgDialog.Destroy()
}

func (a *Application) reset() {
	a.selectedStorageRequest = nil
	a.resultsBox.Reset()
	a.urlEntry.SetText("")
	a.methodCombo.SetActive(0)
	a.queryValueList.Reset()
	a.headerValueList.Reset()
	a.bodyBox.Reset()
	a.resultsBox.Reset()
	a.authBox.Reset()
}

func (a *Application) guiUpdate() {
	a.saveGuiRequest(nil)
}

func (a *Application) goButton() {
	guiRequest := a.uiToRequest()
	log.Println(guiRequest)
	a.resultsBox.SetBusySpinner(true)
	a.saveGuiRequest(&guiRequest)

	activeEnv := make(map[string]string)
	selectedEnv := a.envCombobox.GetSelectedEnv()
	if selectedEnv != nil {
		if selectedEnv.ParentID != 0 {
			if parent, err := a.storageManager.LoadEnvironment(selectedEnv.ParentID); err != nil {
				log.Println("error loading parent", err)
			} else {
				selectedEnv.Parent = &parent
			}
		}
		activeEnv = selectedEnv.GetActiveEnv()
	}

	go func() {
		response := requestor.MakeRequest(guiRequest, activeEnv)

		glib.IdleAdd(func() {
			a.requestComplete(response)
		})
	}()
}

func (a *Application) requestComplete(response models.GuiResponse) {
	if a.storageManager != nil && a.selectedStorageRequest != nil {
		if id, err := a.storageManager.SaveResponse(a.selectedStorageRequest.ID, response); err != nil {
			log.Println(err)
		} else {
			response.ResponseID = id
		}
	}
	a.resultsBox.UpdateForResponse(response, a.selectedStorageRequest)
}

func (a *Application) uiToRequest() models.GuiRequest {

	guiRequest := models.GuiRequest{}
	url, _ := a.urlEntry.GetText()
	guiRequest.URL = url
	guiRequest.Method = a.methodCombo.GetMethod()
	guiRequest.Query = a.queryValueList.GetPairs()
	guiRequest.Headers = a.headerValueList.GetPairs()
	guiRequest.BodyType = a.bodyBox.GetBodyType()
	guiRequest.BodyRaw, _ = a.bodyBox.GetRawBody()
	guiRequest.BodyPairs = a.bodyBox.GetKVPairs()
	guiRequest.AuthType = a.authBox.GetAuthType()
	guiRequest.Auth = a.authBox.GetValues()
	notesBuff, err := a.notes.GetBuffer()
	if err == nil {
		guiRequest.Notes, _ = notesBuff.GetText(notesBuff.GetStartIter(), notesBuff.GetEndIter(), false)
	}
	guiRequest.FilePath, _ = a.bodyBox.GetFilePath()
	guiRequest.FileKey, _ = a.bodyBox.GetFileKey()

	return guiRequest
}

func (a *Application) saveGuiRequest(guiRequest *models.GuiRequest) {
	if a.selectedStorageRequest != nil && a.storageManager != nil && !a.isUpdating {
		if guiRequest == nil {
			fresh := a.uiToRequest()
			guiRequest = &fresh
		}
		a.selectedStorageRequest.GuiRequest = guiRequest
		err := a.storageManager.UpdateRequest(*a.selectedStorageRequest)
		if err != nil {
			log.Println("error saving storage request", err)
		}
	}
}

func (a *Application) ListResponses() ([]models.GuiResponse, error) {
	if a.storageManager == nil || a.selectedStorageRequest == nil {
		return nil, nil
	}
	return a.storageManager.ListResponsesForRequest(a.selectedStorageRequest.ID)
}

func (a *Application) ResponsesForRequestReverse(limit int) ([]models.GuiResponse, error) {
	if a.storageManager == nil || a.selectedStorageRequest == nil {
		return nil, nil
	}
	return a.storageManager.ResponseForRequestReverse(a.selectedStorageRequest.ID, limit)
}

func (a *Application) DeleteResponse(response models.GuiResponse) error {
	return a.storageManager.DeleteResponse(response)
}

func (a *Application) ClearAllResponsesForRequest() error {
	return a.storageManager.ClearAllResponsesForRequest(a.selectedStorageRequest.ID)
}

func (a *Application) ResponseSelected(response models.GuiResponse) {
	a.resultsBox.UpdateForResponse(response, a.selectedStorageRequest)
}

func (a *Application) GetBodyFilePath(key string) string {
	fd, err := gtk.FileChooserNativeDialogNew("Request File", a.mainWindow, gtk.FILE_CHOOSER_ACTION_OPEN, "Open", "Cancel")
	if err != nil {
		a.showErrorDialog(err.Error())
		return ""
	}
	defer fd.Destroy()

	response := fd.Run()
	if response == int(gtk.RESPONSE_ACCEPT) {
		return fd.GetFilename()
	}
	return ""
}

func (a *Application) DoAddProject(project models.Project) int64 {
	id, err := a.storageManager.AddProject(project)
	if err != nil {
		a.showErrorDialog(err.Error())
		return -1
	}
	return id
}

func (a *Application) DoAddFolder(folder models.Folder) int64 {
	id, err := a.storageManager.AddFolder(folder)
	if err != nil {
		a.showErrorDialog(err.Error())
		return -1
	}
	return id
}

func (a *Application) SetProjectSelected(project models.Project) {
	folders, err := a.storageManager.ListFolders(project.ID)
	if err != nil {
		a.showErrorDialog(err.Error())
		return
	}
	a.projectBox.SetFolders(folders)
}

func (a *Application) DoAddRequest(request models.StorageRequest, projectID int64) int64 {
	id, err := a.storageManager.AddRequest(request)
	if err != nil {
		a.showErrorDialog(err.Error())
		return -1
	}
	return id
}

func (a *Application) SetRequestSelected(request models.StorageRequest) {
	a.isUpdating = true
	defer func() { a.isUpdating = false }()
	a.selectedStorageRequest = nil
	a.reset()
	a.selectedStorageRequest = &request
	guiRequest, err := a.storageManager.LoadGuiRequest(a.selectedStorageRequest.ID)
	if err != nil {
		log.Println(err)
		a.showErrorDialog(err.Error())
		return
	}
	a.selectedStorageRequest.GuiRequest = &guiRequest
	a.showSelectedRequest()
}

func (a *Application) showSelectedRequest() {
	a.headerBar.SetSubtitle(a.selectedStorageRequest.Name)
	if a.selectedStorageRequest.GuiRequest.URL != "" {
		a.urlEntry.SetText(a.selectedStorageRequest.GuiRequest.URL)
	}
	a.methodCombo.UpdateActive(a.selectedStorageRequest.GuiRequest.Method)
	a.queryValueList.UpdatePairs(a.selectedStorageRequest.GuiRequest.Query)
	a.headerValueList.UpdatePairs(a.selectedStorageRequest.GuiRequest.Headers)
	a.bodyBox.UpdateBody(a.selectedStorageRequest.GuiRequest.BodyType, a.selectedStorageRequest.GuiRequest.BodyPairs,
		a.selectedStorageRequest.GuiRequest.BodyRaw, a.selectedStorageRequest.GuiRequest.FilePath, a.selectedStorageRequest.GuiRequest.FileKey)
	a.authBox.UpdateAuth(a.selectedStorageRequest.GuiRequest.AuthType, a.selectedStorageRequest.GuiRequest.Auth)

	notesBuffer, err := a.notes.GetBuffer()
	if err == nil {
		notesBuffer.SetText(a.selectedStorageRequest.GuiRequest.Notes)
	}
	a.entryNotebook.SetCurrentPage(0)

	gr, err := a.storageManager.GetLatestResponse(a.selectedStorageRequest.ID)
	if err == nil {
		a.resultsBox.UpdateForResponse(gr, a.selectedStorageRequest)
	}
}

func (a *Application) DoFolderDelete(folder models.Folder, projectID int64) {
	a.storageManager.DeleteFolder(folder)
	folders, err := a.storageManager.ListFolders(projectID)
	if err != nil {
		a.showErrorDialog(err.Error())
		return
	}
	a.projectBox.SetFolders(folders)
}

func (a *Application) DoRequestDelete(request models.StorageRequest, projectID int64) {
	a.storageManager.DeleteRequest(request)
	folders, err := a.storageManager.ListFolders(projectID)
	if err != nil {
		a.showErrorDialog(err.Error())
		return
	}
	a.projectBox.SetFolders(folders)
}

func (a *Application) DoUpdateFolder(folder models.Folder) bool {
	a.storageManager.UpdateFolder(folder)
	return true
}

func (a *Application) DoUpdateProject(project models.Project) bool {
	a.storageManager.UpdateProject(project)
	return true
}

func (a *Application) DoUpdateRequest(request models.StorageRequest) bool {
	a.storageManager.UpdateRequest(request)
	return true
}

func panicIf(err error) {
	if err != nil {
		panic(err)
	}
}
