package storage

import (
	"database/sql"
	"encoding/json"
	"log"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/kendellfab/restscope/internal/models"
)

const (
	isoFormat = "2006-01-02T15:04:05.999999999"
)

type Manager struct {
	db *sql.DB
}

func NewManager(path string) (*Manager, error) {
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec("PRAGMA journal_mode = memory;")
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`create table if not exists project(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL
        );`)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`create table if not exists folder(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            project_id INTEGER
        );`)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`create table if not exists storagerequest(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            folder_id INTEGER,
            guirequest TEXT
        );`)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`create table if not exists environment(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            pairs TEXT NOT NULL,
            parentid INTEGER
        );`)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`create table if not exists response(
            id INTEGER PRIMARY KEY,
            request_id INTEGER,
            at DATETIME,
            response TEXT,
            code INTEGER,
            duration INTEGER
        );`)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`alter table environment add column parentid integer default 0;`)
	if err != nil {
		if !strings.Contains(err.Error(), "duplicate column name") {
			return nil, err
		}
	}

	return &Manager{db: db}, nil
}

func (m *Manager) AddProject(project models.Project) (int64, error) {
	stmt, err := m.db.Prepare("INSERT INTO project(name) VALUES(?);")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(project.Name)
	if err != nil {
		return -1, err
	}

	return res.LastInsertId()
}

func (m *Manager) UpdateProject(project models.Project) error {
	stmt, err := m.db.Prepare("UPDATE project SET name = ? WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(project.Name, project.ID)
	return err
}

func (m *Manager) DeleteProject(project models.Project) error {
	stmt, err := m.db.Prepare("DELETE FROM project WHERE id = :id;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(project.ID)
	return err
}

func (m *Manager) ListProjects() ([]models.Project, error) {
	stmt, err := m.db.Prepare("SELECT id, name FROM project;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	var res []models.Project
	rows, err := stmt.Query()
	if err != nil {
		return res, err
	}

	for rows.Next() {
		var proj models.Project
		err = rows.Scan(&proj.ID, &proj.Name)
		if err == nil {
			res = append(res, proj)
		} else {
			log.Println("error scanning project row", err)
		}
	}
	return res, nil
}

func (m *Manager) AddFolder(folder models.Folder) (int64, error) {
	stmt, err := m.db.Prepare("INSERT INTO folder(name, project_id) VALUES(?, ?);")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(folder.Name, folder.ProjectID)
	if err != nil {
		return -1, err
	}
	return res.LastInsertId()
}

func (m *Manager) UpdateFolder(folder models.Folder) error {
	stmt, err := m.db.Prepare("UPDATE folder SET name = ? WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(folder.Name, folder.ID)
	return err
}

func (m *Manager) DeleteFolder(folder models.Folder) error {
	stmt, err := m.db.Prepare("DELETE FROM folder WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(folder.ID)
	return err
}

func (m *Manager) ListFolders(projectID int64) ([]models.Folder, error) {
	stmt, err := m.db.Prepare("SELECT id, name FROM folder WHERE project_id = ?;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	var res []models.Folder
	rows, err := stmt.Query(projectID)
	if err != nil {
		return res, err
	}
	for rows.Next() {
		var folder models.Folder
		if err = rows.Scan(&folder.ID, &folder.Name); err == nil {
			folder.ProjectID = projectID
			var reqErr error
			folder.Requests, reqErr = m.ListStorageRequests(folder.ID)
			if reqErr != nil {
				log.Println("error loading requests for folder", folder.Name)
			}
			res = append(res, folder)
		} else {
			log.Println("error scanning folder row", err)
		}
	}
	return res, nil
}

func (m *Manager) AddRequest(storageRequest models.StorageRequest) (int64, error) {
	stmt, err := m.db.Prepare("INSERT INTO storagerequest(name, folder_id, guirequest) VALUES(?, ?, ?);")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	gui, err := json.Marshal(storageRequest.GuiRequest)
	if err != nil {
		return -1, err
	}

	res, err := stmt.Exec(storageRequest.Name, storageRequest.FolderID, string(gui))
	if err != nil {
		return -1, err
	}
	return res.LastInsertId()
}

func (m *Manager) DeleteRequest(storageRequest models.StorageRequest) error {
	stmt, err := m.db.Prepare("DELETE from storagerequest WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(storageRequest.ID)
	return err
}

func (m *Manager) DeleteRequestsByFolder(folderID int64) error {
	stmt, err := m.db.Prepare("DELETE FROM storagerequest WHERE folder_id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(folderID)
	return err
}

func (m *Manager) UpdateRequest(req models.StorageRequest) error {
	stmt, err := m.db.Prepare("UPDATE storagerequest SET name = ?, guirequest = ? WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	gui, err := json.Marshal(req.GuiRequest)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(req.Name, string(gui), req.ID)
	return err
}

func (m *Manager) LoadGuiRequest(storageRequestID int64) (models.GuiRequest, error) {
	stmt, err := m.db.Prepare("SELECT guirequest FROM storagerequest WHERE id = ?;")
	if err != nil {
		return models.GuiRequest{}, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(storageRequestID)
	var guiRequestData string
	err = row.Scan(&guiRequestData)
	if err != nil {
		return models.GuiRequest{}, err
	}

	var guiRequest models.GuiRequest
	err = json.Unmarshal([]byte(guiRequestData), &guiRequest)
	return guiRequest, err
}

func (m *Manager) ListStorageRequests(folderID int64) ([]models.StorageRequest, error) {
	stmt, err := m.db.Prepare("SELECT id, name FROM storagerequest WHERE folder_id = ?;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(folderID)
	if err != nil {
		return nil, err
	}

	var res []models.StorageRequest
	for rows.Next() {
		var req models.StorageRequest
		if err = rows.Scan(&req.ID, &req.Name); err == nil {
			req.FolderID = folderID
			res = append(res, req)
		} else {
			log.Println("error scanning storag request row", err)
		}
	}
	return res, nil
}

func (m *Manager) AddEnvironment(env models.Environment) (int64, error) {
	stmt, err := m.db.Prepare("INSERT INTO environment(name, pairs, parentid) VALUES(?, ?, ?);")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	pairsData, err := json.Marshal(env.Pairs)
	if err != nil {
		return -1, err
	}

	res, err := stmt.Exec(env.Name, string(pairsData), env.ParentID)
	if err != nil {
		return -1, err
	}
	return res.LastInsertId()
}

func (m *Manager) UpdateEnvironment(env models.Environment) error {
	stmt, err := m.db.Prepare("UPDATE environment SET name = ?, pairs = ? WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	pairsData, err := json.Marshal(env.Pairs)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(env.Name, string(pairsData), env.ID)
	return err
}

func (m *Manager) ListHierarchy() ([]models.Environment, error) {
	stmt, err := m.db.Prepare("SELECT id, name, pairs FROM environment WHERE parentid = 0;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}

	var res []models.Environment
	for rows.Next() {
		var pairs string
		var env models.Environment
		if err = rows.Scan(&env.ID, &env.Name, &pairs); err == nil {
			pairErr := json.Unmarshal([]byte(pairs), &env.Pairs)
			if pairErr != nil {
				log.Println("error unmarshalling parent pairs", pairErr)
			}
			if children, childrenErr := m.ListChildren(env.ID); childrenErr == nil {
				env.Children = children
			} else {
				log.Println("error adding children", childrenErr)
			}

			res = append(res, env)
		}
	}
	return res, nil
}

func (m *Manager) ListChildren(parentID int64) ([]models.Environment, error) {
	stmt, err := m.db.Prepare("SELECT id, name, pairs FROM environment WHERE parentid = ?;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(parentID)
	if err != nil {
		return nil, err
	}
	var res []models.Environment
	for rows.Next() {
		var pairs string
		var env models.Environment
		if err = rows.Scan(&env.ID, &env.Name, &pairs); err == nil {
			pairErr := json.Unmarshal([]byte(pairs), &env.Pairs)
			if pairErr != nil {
				log.Println("error unmarshalling child pairs", pairErr)
			}
			env.ParentID = parentID
			res = append(res, env)
		}
	}
	return res, nil
}

func (m *Manager) DeleteEnvironment(env models.Environment) error {
	stmt, err := m.db.Prepare("DELETE FROM environment WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(env.ID)
	return err
}

func (m *Manager) LoadEnvironment(id int64) (models.Environment, error) {
	stmt, err := m.db.Prepare("SELECT id, name, pairs, parentid FROM environment WHERE id = ?;")
	if err != nil {
		return models.Environment{}, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(id)

	var pairs string
	var env models.Environment
	err = row.Scan(&env.ID, &env.Name, &pairs, &env.ParentID)
	if err != nil {
		return models.Environment{}, err
	}

	pairErr := json.Unmarshal([]byte(pairs), &env.Pairs)
	if pairErr != nil {
		log.Println("error unmarshalling env pairs", pairErr)
	}
	return env, nil
}

func (m *Manager) SaveResponse(requestID int64, response models.GuiResponse) (int64, error) {
	stmt, err := m.db.Prepare("INSERT INTO response(request_id, at, response, code, duration) VALUES(?, ?, ?, ?, ?);")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	data, err := json.Marshal(response)
	if err != nil {
		return -1, err
	}

	now := time.Now()

	res, err := stmt.Exec(requestID, now.Format(isoFormat), string(data), response.Code, response.ConnectTime+response.RecvTime)
	if err != nil {
		return -1, err
	}
	return res.LastInsertId()
}

func (m *Manager) GetLatestResponse(requestID int64) (models.GuiResponse, error) {
	stmt, err := m.db.Prepare("SELECT response FROM response WHERE request_id = ? ORDER BY at DESC LIMIT 1;")
	if err != nil {
		return models.GuiResponse{}, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(requestID)
	if err != nil {
		return models.GuiResponse{}, err
	}
	defer rows.Close()

	rows.Next()

	var respData string
	var resp models.GuiResponse

	err = rows.Scan(&respData)
	if err != nil {
		return models.GuiResponse{}, err
	}

	err = json.Unmarshal([]byte(respData), &resp)
	return resp, err
}

func (m *Manager) ListResponsesForRequest(requestID int64) ([]models.GuiResponse, error) {
	stmt, err := m.db.Prepare("SELECT id, at, response FROM response WHERE request_id = ? ORDER BY at DESC;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(requestID)
	if err != nil {
		return nil, err
	}

	return m.parseResponses(rows)
}

func (m *Manager) ResponseForRequestReverse(requestID int64, limit int) ([]models.GuiResponse, error) {
	stmt, err := m.db.Prepare("SELECT id, at, response FROM response WHERE request_id = ? ORDER BY at DESC LIMIT ?;")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query(requestID, limit)
	if err != nil {
		return nil, err
	}

	return m.parseResponses(rows)
}

func (m *Manager) parseResponses(rows *sql.Rows) ([]models.GuiResponse, error) {
	var res []models.GuiResponse
	var err error
	for rows.Next() {
		var resp models.GuiResponse
		var respData string
		if err = rows.Scan(&resp.ResponseID, &resp.At, &respData); err == nil {
			if err = json.Unmarshal([]byte(respData), &resp); err == nil {
				res = append(res, resp)
			} else {
				log.Println("error unmarshalling gui response", err)
			}
		} else {
			log.Println("error scanning gui response", err)
		}
	}
	return res, err
}

func (m *Manager) DeleteResponse(response models.GuiResponse) error {
	stmt, err := m.db.Prepare("DELETE FROM response WHERE id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(response.ResponseID)
	return err
}

func (m *Manager) ClearAllResponsesForRequest(requestID int64) error {
	stmt, err := m.db.Prepare("DELETE FROM response WHERE request_id = ?;")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(requestID)
	return err
}

func (m *Manager) Close() {
	m.db.Close()
}
