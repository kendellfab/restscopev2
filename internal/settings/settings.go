package settings

type Settings struct {
	LastDB                string `json:"lastDB"`
	SchemeID              string `json:"schemeID"`
	SelectedEnvironmentID int64  `json:"selectedEnvironmentID"`
}
