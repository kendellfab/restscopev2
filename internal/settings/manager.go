package settings

type Manager interface {
	GetLastDB() (string, error)
	SetLastDB(value string) error
	GetSchemeID() (string, error)
	SetSchemeID(schemeID string)
	GetSelectedEnvironmentID() (int64, error)
	SetSelectedEnvironmentID(envID int64)
}
