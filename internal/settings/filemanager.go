package settings

import (
	"encoding/json"
	"log"
	"os"
	"os/user"
	"path/filepath"
)

type FileManager struct {
	rootPath     string
	settingsPath string
	settings     Settings
}

func (f *FileManager) GetLastDB() (string, error) {
	return f.settings.LastDB, nil
}

func (f *FileManager) SetLastDB(value string) error {
	f.settings.LastDB = value
	f.saveSettings()
	return nil
}

func (f *FileManager) GetSchemeID() (string, error) {
	return f.settings.SchemeID, nil
}

func (f *FileManager) SetSchemeID(schemeID string) {
	f.settings.SchemeID = schemeID
	f.saveSettings()
}

func (f *FileManager) GetSelectedEnvironmentID() (int64, error) {
	return f.settings.SelectedEnvironmentID, nil
}

func (f *FileManager) SetSelectedEnvironmentID(envID int64) {
	f.settings.SelectedEnvironmentID = envID
	f.saveSettings()
}

func (f *FileManager) saveSettings() {
	w, err := os.Create(f.settingsPath)
	if err == nil {
		defer w.Close()
		encErr := json.NewEncoder(w).Encode(f.settings)
		if encErr != nil {
			log.Println("error encoding settings", encErr)
		}
	}
}

func NewFileManager() Manager {
	usr, err := user.Current()
	if err != nil {
		panic(err)
	}

	rootPath := filepath.Join(usr.HomeDir, ".config", "restscope")
	_, err = os.Stat(rootPath)
	if os.IsNotExist(err) {
		createErr := os.Mkdir(rootPath, os.ModePerm)
		if createErr != nil {
			log.Println(createErr)
		}
	}

	settingsPath := filepath.Join(rootPath, "settings.json")

	var settings Settings
	in, err := os.Open(settingsPath)
	if err == nil {
		defer in.Close()
		decErr := json.NewDecoder(in).Decode(&settings)
		if decErr != nil {
			log.Println("Error decoding settings", decErr)
		}
	} else {
		os.Create(settingsPath)
	}

	return &FileManager{rootPath: rootPath, settingsPath: settingsPath, settings: settings}
}
