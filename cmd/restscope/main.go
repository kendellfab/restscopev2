package main

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/restscope/internal"
	"gitlab.com/kendellfab/restscope/internal/settings"
)

func main() {

	settingsManager := settings.NewFileManager()

	app, _ := gtk.ApplicationNew("us.kendellfabrici.restscope", glib.APPLICATION_FLAGS_NONE)

	//Shows an application as soon as the app starts
	app.Connect("activate", func() {
		application := internal.NewApplication("RestScope", app, settingsManager)
		application.Display(1700, 850)
	})

	app.Run(nil)
}
