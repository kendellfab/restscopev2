module gitlab.com/kendellfab/restscope

go 1.15

require (
	github.com/atotto/clipboard v0.1.4
	github.com/aymerick/raymond v2.0.2+incompatible
	github.com/gotk3/gotk3 v0.6.1
	github.com/hfmrow/gotk3_gtksource v0.9.11
	github.com/jung-kurt/gofpdf v1.16.2 // indirect
	github.com/mattn/go-sqlite3 v1.14.16
	golang.org/x/image v0.1.0 // indirect
	gonum.org/v1/plot v0.12.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
